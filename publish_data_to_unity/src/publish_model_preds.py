#!/usr/bin/env python3
import argparse
import os
DIR = os.path.dirname(os.path.realpath(__file__))
import cv2
import numpy as np
import tensorflow as tf

import sys
print(os.path.join(os.path.dirname(sys.path[0]),"../model/"))
sys.path.insert(0, os.path.join(os.path.dirname(sys.path[0]),"../model/"))

import rospy
from geometry_msgs.msg import Pose
from publish_data_to_unity.msg import HagglingPoses, PepperCameraImage

from cmVAE_model import cmVAE_model
from feedforward_cnn import FeedforwardCNN
from load_data import LoadData

# must use a GPU
device_name = tf.test.gpu_device_name()
physical_devices = tf.config.list_physical_devices('GPU') # use tf.config.list_physical_devices('GPU')
if not physical_devices:
  raise SystemError('GPU device not found')
print('Found GPU at: {}'.format(device_name))
# prevent a process from taking up all gpu memory
tf.config.experimental.set_memory_growth(physical_devices[0], True)

# Copied from parse_haggling_rosbag
def process_img(raw_img):
    curr_img = cv2.imdecode(np.frombuffer(raw_img, dtype = np.uint8), -1)
    gray_img = cv2.cvtColor(curr_img, cv2.COLOR_BGR2GRAY)
    # print('Gray img Dimensions : ', gray_img.shape) # (960, 1280)
    scale_percent = 10 # percent of original size
    width = int(gray_img.shape[1] * scale_percent / 100)
    height = int(gray_img.shape[0] * scale_percent / 100)
    dim = (width, height)
    resized_img = cv2.resize(gray_img, dim, interpolation = cv2.INTER_AREA)
    return np.asarray(resized_img)

"""
load model, subscribe to cam img, and publish model prediction where:

set is_model_prediction to true
store (0, face_orient, 0) and (0, body_orient, 0) as pose.position in ros msg fields "formation" & "faces"

"""
class PublishModelPreds(object):
    def __init__(self, test_model, model, window_len=5):
        self.test_model = test_model
        self.model = model
        self.window_len = window_len
        # Define the publisher and subscriber
        self.pub = rospy.Publisher("/publish_haggling_poses/poses", HagglingPoses, queue_size=10)
        self.sub = rospy.Subscriber("pepper_camera/output", PepperCameraImage, self.publish_pred, queue_size=10)
        # Image queue
        self.images = []
        self.frame = None

        rospy.init_node("publish_model_preds")

        # Run until the node is interrupted
        rospy.spin()

    def getPose(self, angle):
        curr_pose = Pose()
        curr_pose.position.x = 0
        curr_pose.position.y = angle
        curr_pose.position.z = 0
        return curr_pose

    # Callback function for a new message
    def publish_pred(self, message):
        # Process the first image of every frame
        if self.frame == message.frame_num:
            return
        self.frame = message.frame_num
        self.images.append(process_img(message.image.data))

        # Ignore the first window_len time steps
        if len(self.images) < self.window_len:
            return
        elif len(self.images) > self.window_len:
            self.images.pop(0)

        inputImage = LoadData.generate_input(self.images, int(message.haggler_status))
        if self.model == "cnn":
            (body_angle, face_angle) = self.test_model(inputImage)[0]
        elif self.model == "cmvae":
            img_recon, y_pred, means, stddev, z = self.test_model(inputImage)
            (body_angle, face_angle) = y_pred[0]

        # Build the new message
        new_msg = HagglingPoses()
        new_msg.header.stamp = rospy.get_rostime()
        new_msg.frame_num = message.frame_num
        new_msg.is_model_prediction = True
        new_msg.formation.poses.append(self.getPose(body_angle.numpy()))
        new_msg.faces.poses.append(self.getPose(face_angle.numpy()))

        print(f"Sending pred {body_angle}, {face_angle} (body, face) for frame {message.frame_num}")
        self.pub.publish(new_msg)


def main():
    parser = argparse.ArgumentParser(description="Publish trained model's prediction for robot face and body rotation")
    parser.add_argument('--best_model', type=str, default='cmvae_04-10-2021-17-40_mse_adam', help="name of model to load and test")
    parser.add_argument('--model', choices=['cmvae', 'cnn'], default='cmvae')
    parser.add_argument('--latent_dim', type=int, default=16)
    parser.add_argument('--model_type', type=int, default=2, help="adjust cmvae encoder/decoder structure; 1, 2, 3, 4, 5")
    parser.add_argument('--mode', type=int, default=4, help="0--train everything; 4--train everything but pass distribution to action decoder instead of sampled z vector; 1, 2, 3--freeze encoder / decoder; see cmBAE.py")
    
    args = parser.parse_args()

    model  = args.model
    best_model  = args.best_model
    latent_dim = args.latent_dim
    model_type = args.model_type
    mode= args.mode

    if args.model == "cmvae":
        # hyperparameters
        hps = {
            "latent_dims": latent_dim,
            "model_type": model_type,
            "mode": mode,
        }
        test_model = cmVAE_model(hps)
        weights_path = f"{DIR}/../../model/logs/{best_model}/best_action_weights.ckpt"
        print(f"weights_path: {weights_path}")
        test_model.load_weights(weights_path)
        print("finished loading weights")
    elif args.model == "cnn":
        test_model = FeedforwardCNN()
        test_model.compile(
            optimizer='adam',
            loss=tf.keras.losses.MeanSquaredError(),
            metrics=[
                'MeanSquaredError',
            ]
        )
        weights_path = f"{DIR}/../../model/logs/{best_model}/best_model.ckpt"
        test_model.load_weights(weights_path)
        print("finished loading CNN model")
    else:
        print(f"unsupported model type: {args.format}")
        exit()

    try:
        node = PublishModelPreds(test_model, args.model)
    except rospy.ROSInterruptException:
        pass


if __name__ == "__main__":
    main()
