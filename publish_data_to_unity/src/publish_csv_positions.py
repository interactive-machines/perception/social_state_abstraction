#!/usr/bin/env python
'''
publish cocktail party positions to unity

download the following into the `data` folder:
  - `grouping.ref` -- https://drive.google.com/file/d/0Bzf1l8WmTwu0ZUdMOFRiZ1JyS3M/view
  - `tracking_hp.log` --  https://drive.google.com/file/d/0Bzf1l8WmTwu0ZWxZdVZ0RUFFNm8/view
'''
import numpy as np
import os
import rospy
import tf.transformations as tr
import csv
import cv2
import rospkg

from sensor_msgs.msg import CompressedImage

r = rospkg.RosPack()
ROOT_DIR = r.get_path("publish_data_to_unity")

# Uncomment to put root as current dir
# ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

from publish_data_to_unity.msg import TrackedPerson, TrackedPersons


class PublishPositions(object):
    def __init__(self):
        rospy.init_node("publish_pilot_positions")

        self.data_folder = os.path.join(ROOT_DIR, rospy.get_param('~data_folder', 'data'))
        print("data folder: {}".format(self.data_folder))

        self.people_pub = rospy.Publisher("/publish_positions/tracked_persons", TrackedPersons, queue_size=1)

        self.image_index = 0
        self.publish_time = None
        self.root_output = rospy.get_param('~root_output', 'recordings/male/real')
        self.camera = rospy.get_param('~camera', 'top')
        if not os.path.isdir(self.root_output):
            os.makedirs(self.root_output)
        print("root output: " + self.root_output)
        top_path = os.path.join(self.root_output, 'top')
        side_path = os.path.join(self.root_output, 'side')
        low_path = os.path.join(self.root_output, 'low')

        self.top_pub = rospy.Subscriber("/high_camera_feed", CompressedImage, self.callback(top_path), queue_size=1)
        self.side_pub = rospy.Subscriber("/side_camera_feed", CompressedImage, self.callback(side_path), queue_size=1)
        self.low_pub = rospy.Subscriber("/low_camera_feed", CompressedImage, self.callback(low_path), queue_size=1)


        self.init_time = rospy.Time.now()
        self.csv = rospy.get_param('~csv', 'render_pilot.csv')

        self.set_flags(init=True)

        # while self.side_pub.get_num_connections() == 0:
        #     print('-')
        #     rospy.sleep(0.2)

    def set_flags(self, init=False):
        self.top = True
        self.side = True
        self.low = True
        if not init:
            setattr(self, self.camera, False)

    def callback(self, location):
        def cb(data):
            if self.publish_time is None:
                return
            diff = (rospy.Time.now() - self.publish_time).to_sec()
            sub_bool = location.split("/")[-1]
            if not getattr(self, sub_bool):
                np_arr = np.fromstring(data.data, np.uint8)
                image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
                image_string = '{}_{}.jpg'.format(location, self.image_index)
                cv2.imwrite(image_string, image_np)
                print("Wrote {}".format(image_string))
                setattr(self, sub_bool, True)
        return cb

    def publish_all(self):
        csv_name = self.csv
        with open(os.path.join(self.data_folder, csv_name)) as csvfile:
            for row in csv.DictReader(csvfile):
                people = TrackedPersons()
                people.header.stamp = rospy.Time.now()

                self.image_index += 1
                num_ppl = int(row['num_people'])
                print("image {}, {} people".format(self.image_index, num_ppl))

                for track_id in range(num_ppl):
                    x = float(row['p{}_x'.format(track_id)])
                    y = float(row['p{}_y'.format(track_id)])
                    th = float(row['p{}_theta'.format(track_id)])

                    track = TrackedPerson()
                    track.track_id = track_id + 1
                    track.pose.position.x = x
                    track.pose.position.y = y
                    track.pose.position.z = 0.1
                    R = tr.rotation_matrix(th, (0,0,1))
                    q = tr.quaternion_from_matrix(R)
                    track.pose.orientation.x = q[0]
                    track.pose.orientation.y = q[1]
                    track.pose.orientation.z = q[2]
                    track.pose.orientation.w = q[3]
                    people.tracks.append(track)

                self.people_pub.publish(people)

                self.publish_time = rospy.Time.now()

                rospy.sleep(2)

                while (rospy.Time.now() - self.init_time).to_sec() <= 12:
                    rospy.loginfo_throttle(5, "Waiting for initialization in Unity...")

                self.set_flags()

                while not getattr(self, self.camera):
                    rospy.loginfo_throttle(5, "Waiting to save all image views...")


if __name__ == "__main__":
    try:
        node = PublishPositions()
        rospy.sleep(5)
        node.publish_all()
    except rospy.ROSInterruptException:
        pass

