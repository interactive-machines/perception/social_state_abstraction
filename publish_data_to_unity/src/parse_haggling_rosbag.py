#!/usr/bin/env python3

import os
import numpy as np
import rosbag
import argparse

"""
one npz file for the whole dataset, split into sequences

"""
parser = argparse.ArgumentParser(description='Save rosbag in npz format')
parser.add_argument('--bagname', type=str, default='cocktail_party_2021-04-01-23-56-10.bag',
                    help='name / path to rosbag')
parser.add_argument('--filename', type=str, default='haggling_dataset.npz',
                    help='name of npz file')
args = parser.parse_args()

bag = rosbag.Bag(args.bagname)
file_name = args.filename

prev_frame = -1
dataset = []
data_seq = []

def save_data_seq():
    global data_seq, seq_counter
    if len(data_seq) <= 1:
        return
    print(len(data_seq))
    dataset.append(data_seq)
    data_seq = []

skipped_first_frame = False
for topic, msg, time in bag.read_messages(topics=['/pepper_camera/output']):
    if msg.frame_num == prev_frame:
        continue
    elif prev_frame > -1 and msg.frame_num != prev_frame + 1:
        save_data_seq()
        skipped_first_frame = False
    elif msg.haggling and len(data_seq) > 0:
        save_data_seq()
        skipped_first_frame = False
    prev_frame = msg.frame_num
    # filter out data when robot is speaking
    if not msg.haggling:
        # skip first data frame of each data sequence
        if not skipped_first_frame:
            skipped_first_frame = True
        else:
            data_seq.append((msg.image.data, msg.face_orient, msg.body_orient, msg.image.header.stamp, msg.haggling_msg_stamp, msg.haggler_status))

np.savez(file_name, dataset)

bag.close()

