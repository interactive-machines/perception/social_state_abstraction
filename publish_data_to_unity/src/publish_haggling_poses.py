#!/usr/bin/env python
'''
publish the haggling poses to unity
'''
import actionlib
import argparse
import errno
import numpy as np
import os
import re
import rospy
import json
import pickle
import math
import tf.transformations as tr

from geometry_msgs.msg import Pose, PoseArray
from publish_data_to_unity.msg import HagglingPoses


# ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
HOME_DIR = os.path.expanduser("~")
RESCALE = 90.0
BODY_DATA_PATH = HOME_DIR + '/ssp/data/haggling/panopticDB_body_pkl_hagglingProcessed/'
SPEECH_DATA_PATH = HOME_DIR + '/ssp/data/haggling/panopticDB_speech_pkl_hagglingProcessed/'
FRAME_RATE = 10 # 30Hz but slower for data collection

class PublishHagglingPoses(object):
    def __init__(self):
        rospy.init_node("publish_haggling_poses")

        self.pub = rospy.Publisher("/publish_haggling_poses/poses", HagglingPoses, queue_size=10)

        #rospy.spin()


    def read_data(self, filename):
        # motion_data = pickle.load( open( filepath, "rb" ) , encoding='latin1')
        # python2:
        body_data_path = BODY_DATA_PATH + filename
        # print("body_data_path: {}".format(body_data_path))
        motion_data = pickle.load( open( body_data_path, "rb" ))
        self.ava_joints = np.array([motion_data['subjects'][0]['joints19'], motion_data['subjects'][1]['joints19'], motion_data['subjects'][2]['joints19']])
        # each is (57, 1773)
        self.ava_bodynorms = np.array([motion_data['subjects'][0]['bodyNormal'], motion_data['subjects'][1]['bodyNormal'], motion_data['subjects'][2]['bodyNormal']])
        self.ava_facenorms = np.array([motion_data['subjects'][0]['faceNormal'], motion_data['subjects'][1]['faceNormal'], motion_data['subjects'][2]['faceNormal']])
        # each is (3, 1773), with y axis = 0

        # defines connections between joint indices that form bones
        # [6,7], [7,8], [9,10], [10, 11]
        self.bones = [[0,1], [0,3], [3,4], [4,5], [0,2], [2,6], [6,7], [7,8], [2,12],[12,13], [13,14], [0,9], [9, 10], [10, 11]]

        speech_data_path = SPEECH_DATA_PATH + filename
        # print("speech_data_path: {}".format(speech_data_path))
        speech_data = pickle.load( open( speech_data_path, "rb" ))
        self.ava_speech = np.array([speech_data['speechData'][0]['indicator'], speech_data['speechData'][1]['indicator'], speech_data['speechData'][2]['indicator']])
        # determine who the buyer is (silent the most)
        ava0_speaknums = np.count_nonzero(self.ava_speech[0])
        ava1_speaknums = np.count_nonzero(self.ava_speech[1])
        ava2_speaknums = np.count_nonzero(self.ava_speech[2])
        min_num = min(ava0_speaknums, ava1_speaknums, ava2_speaknums)
        self.buyer_id = 0
        if ava1_speaknums < ava0_speaknums:
            self.buyer_id = 1
        if ava2_speaknums < ava1_speaknums and ava2_speaknums < ava0_speaknums:
            self.buyer_id = 2
        

    def publish_all(self, use_robot_id, test_mode):
        rate = rospy.Rate(FRAME_RATE)

        for frame_idx in range(len(self.ava_joints[0][0])): # num of frames e.g. 1773
            # print("frame_num: {}".format(frame_idx))
            new_msg = HagglingPoses()
            new_msg.header.stamp =  rospy.get_rostime()
            new_msg.frame_num = frame_idx
            new_msg.use_robot_id = use_robot_id
            new_msg.buyer_id = self.buyer_id
            new_msg.is_model_prediction = False
            if test_mode and frame_idx > 5:
                # don't ignore robot for the first 3 frames to give initial position and orientation for test time
                new_msg.ignore_robot = True
            else:
                new_msg.ignore_robot = False

            curr_speech = self.ava_speech[:,frame_idx] # e.g. [0., 1., 0.] float64
            # print("frame_idx: {}, curr_speech: {}".format(frame_idx, curr_speech))
            new_msg.speech_bin = curr_speech

            if len(curr_speech.shape) != 1 or len(curr_speech) != 3:
                print("expected curr_speech to be [0 or 1, 0 or 1, 0 or 1], got: ", curr_speech)
                exit(1)

            for avatar_id in range(len(self.ava_joints)): # 3 avatars
                ## FORMATION
                curr_joints = self.ava_joints[avatar_id][:,frame_idx]
                curr_pos = curr_joints[:3]
                if len(curr_pos.shape) != 1 or len(curr_pos) != 3:
                    print("expected curr_pos to be [x,y,z], got: ", curr_pos)
                    exit(1)
                curr_formation = Pose()
                # rescaling cuz positions are quite big
                curr_formation.position.x = curr_pos[2] / RESCALE # forward is x in ros but z in opengl
                curr_formation.position.y = -curr_pos[0] / RESCALE # left is y in ros but -x in opengl
                curr_formation.position.z = 0 # ignore y axis in position means z axis in ros

                # use body normal for orientation
                curr_bodynorm = self.ava_bodynorms[avatar_id][:,frame_idx]
                if len(curr_bodynorm.shape) != 1 or len(curr_bodynorm) != 3:
                    print("expected curr_bodynorm to be [x,y,z], got: ", curr_bodynorm)
                    exit(1)
                x1 = curr_bodynorm[2]
                y1 = -curr_bodynorm[0] 
                z1 = 0 # ignore y axis (up) means z axis in ros

                body_angle = math.atan2(y1, x1) # axis of rotation is (0,0,1)

                q_body = tr.quaternion_about_axis(body_angle, (0, 0, 1))

                curr_formation.orientation.x = q_body[0]
                curr_formation.orientation.y = q_body[1]
                curr_formation.orientation.z = q_body[2]
                curr_formation.orientation.w = q_body[3]

                new_msg.formation.poses.append(curr_formation)

                ## FACE ORIENTATION
                # use body normal for orientation
                curr_face = Pose()
                curr_face.position = curr_formation.position
                curr_facenorm = self.ava_facenorms[avatar_id][:,frame_idx]
                if len(curr_facenorm.shape) != 1 or len(curr_facenorm) != 3:
                    print("expected curr_facenorm to be [x,y,z], got: ", curr_facenorm)
                    exit(1)
                x1 = curr_facenorm[2]
                y1 = -curr_facenorm[0] 
                z1 = 0 # ignore y axis (up) means z axis in ros

                face_angle = math.atan2(y1, x1) # axis of rotation is (0,0,1)

                q_face = tr.quaternion_about_axis(face_angle, (0, 0, 1))

                curr_face.orientation.x = q_face[0]
                curr_face.orientation.y = q_face[1]
                curr_face.orientation.z = q_face[2]
                curr_face.orientation.w = q_face[3]

                # if avatar_id == 2:
                #     msg = "avatar {}; face angle: {} x1: {}, y1: {}".format(avatar_id, face_angle,
                #                                                        x1, y1)
                #     print(msg)

                new_msg.faces.poses.append(curr_face)


                ## JOINTS
                curr_bodies = PoseArray()

                for index, bone in enumerate(self.bones):
                    curr_joint_pose = Pose()

                    # x0, y0, z0 is the coordinate of the base point
                    x0 = curr_joints[3*bone[0]+2] / RESCALE # z axis is x axis in ros
                    y0 = -curr_joints[3*bone[0]] / RESCALE # x axis is -y in ros
                    z0 = curr_joints[3*bone[0]+1] / RESCALE # y axis is z in ros
                    # x1, y1, z1 is the vector points from the base to the target
                    x1 = curr_joints[3*bone[1]+2] / RESCALE - x0
                    y1 = -curr_joints[3*bone[1]] / RESCALE - y0
                    z1 = curr_joints[3*bone[1]+1] / RESCALE - z0 

                    curr_joint_pose.position.x = x0
                    curr_joint_pose.position.y = y0
                    curr_joint_pose.position.z = z0

                    length = math.sqrt(x1*x1 + y1*y1 + z1*z1)
                    theta = math.acos(x1/length) 
                    phi = math.atan2(z1, y1)
                    # no rotation around x axis; 7 and 10 are shin bones
                    if index == 6 or index == 7 or index == 9 or index == 10:
                        theta *= -1
                        phi *= -1
                    # neck to nose
                    if index == 0:
                        phi *= -1

                    # forearm
                    if index == 3 or index == 13:
                        phi *= -1

                    # upperarm
                    if index == 2 or index == 12:
                        phi *= -1

                    q1 = tr.quaternion_about_axis(theta, (0, 0, 1))
                    q2 = tr.quaternion_about_axis(phi, (1, 0, 0))
                    q_bone = tr.quaternion_multiply(q1, q2)

                    # try multiplying by the bodynormal as well
                    q_bone = tr.quaternion_multiply(q_body, q_bone)

                    curr_joint_pose.orientation.x = q_bone[0]
                    curr_joint_pose.orientation.y = q_bone[1]
                    curr_joint_pose.orientation.z = q_bone[2]
                    curr_joint_pose.orientation.w = q_bone[3]

                    curr_bodies.poses.append(curr_joint_pose)

                new_msg.bodies_all.append(curr_bodies)
                
            self.pub.publish(new_msg)

            if rospy.is_shutdown():
                break

            rate.sleep()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Publish haggling data to Unity')
    parser.add_argument('--debugging', type=bool, default=False,
                        help='Debugging plays one seq w/ one use_robot_id')
    parser.add_argument('--filename', type=str, default='170221_haggling_b2_group1.pkl',
                        help='Haggling filename for debugging')
    parser.add_argument('--size', type=int, default=10,
                        help='how many unique data seqs to record; -1 if full size')
    parser.add_argument('--test_mode', type=bool, default=False,
                        help='whether sending data to test model prediction')

    args = parser.parse_args()

    try:
        node = PublishHagglingPoses()

        all_files = os.listdir(BODY_DATA_PATH)
        
        test_groups = set(['170221_haggling_b1','170221_haggling_b2','170221_haggling_b3','170228_haggling_b1','170228_haggling_b2','170228_haggling_b3'])
        test_files = []
        for filename in all_files:
            if filename[0:18] in test_groups:
                test_files.append(filename)
        test_files = set(test_files)

        if args.debugging:
            filename = args.filename
            use_robot_id = 0
            print("filename: {}, use_robot_id: {}".format(filename, use_robot_id))
            node.read_data(filename)
            node.publish_all(use_robot_id, args.test_mode)

        elif args.test_mode:
            # test model prediction for robot's face and body orientation in real time
            for filename in test_files:
                for use_robot_id in range(3):
                    print("filename: {}, use_robot_id: {}".format(filename, use_robot_id))
                    node.read_data(filename)
                    node.publish_all(use_robot_id, args.test_mode)

        else:
            # send haggling data to collect train data for model
            count = 0
            for filename in all_files:
                if filename in test_files:
                    continue
                else:
                    # loop 3 times s.t. robot iterates thru each pos
                    for use_robot_id in range(3):
                        print("filename: {}, use_robot_id: {}".format(filename, use_robot_id))
                        node.read_data(filename)
                        node.publish_all(use_robot_id, args.test_mode)
                    count += 1
                    if args.size != -1 and count >= args.size:
                       break

    # except rospy.ROSInterruptException:
    except Exception as e:
        print(e)
        pass


