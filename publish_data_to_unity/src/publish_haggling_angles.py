#!/usr/bin/env python
'''
publish the haggling angles to unity for robot only
'''
import actionlib
import errno
import numpy as np
import os
import re
import rospy
import json

from std_msgs.msg import MultiArrayDimension, Float32MultiArray


ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))


class PublishHagglingAngles(object):
    def __init__(self):
        rospy.init_node("publish_haggling_angles")

        self.pub = rospy.Publisher("/publish_haggling_angles/angle", Float32MultiArray, queue_size=10)

        self.read_data('orientation_with_time.json')

        #rospy.spin()


    def read_data(self, filename):
        with open(filename, 'rb') as data_file:
            haggling_data = json.load(data_file)
            self.body_normal_angles = haggling_data['bodyNormal']
            self.face_normal_angles = haggling_data['faceNormal']
            self.timestamp = haggling_data['timestamp']


    def publish_all(self):
        rate = rospy.Rate(10) # 10 Hz just for testing, should increase to 30
        for i in range(len(self.body_normal_angles)):
            floatArr = Float32MultiArray()
            floatArr.data = [self.body_normal_angles[i], self.face_normal_angles[i], self.timestamp[i]]
            self.pub.publish(floatArr)
            if rospy.is_shutdown():
                break
            rate.sleep()


if __name__ == "__main__":
    try:
        node = PublishHagglingAngles()
        node.publish_all()
    except rospy.ROSInterruptException:
        pass
