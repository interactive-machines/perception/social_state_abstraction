import csv
import os
import numpy as np
import argparse

if __name__ == "__main__":
    # Parse datafile and margin of error
    parser = argparse.ArgumentParser()
    parser.add_argument('--datafile', type=str, help='csv file path for group data', default="../data/render_exp.csv")
    parser.add_argument('--error-margin', type=int, help='angle error margin in degrees', default=60)
    args = parser.parse_args()

    with open(args.datafile, "r") as f:
        # Filter out unnecessary csv data
        reader = list(csv.reader(f))
        idx = [reader[0].index(angle) for angle in reader[0]
               if angle.startswith(("p", "num")) and not angle.endswith("id")]
        np_reader = np.array(reader)[1:, idx]

        # Go through each scene
        for index, scene in enumerate(np_reader):

            # Get num people, individual data (x,y,theta) and context data(x,y,theta)
            num_people = scene[0]
            remaining_rows = 4 + 3 * (int(num_people)-1)
            ind = np.array([float(i) for i in scene[1:4]])
            context = [float(i) for i in scene[4:remaining_rows]]

            # Isolate context x,y data
            context_x = [j for i,j in enumerate(context) if i % 3 == 0]
            context_y = [j for i,j in enumerate(context) if i % 3 == 1]

            # Convert individual angle to -pi to pi
            actual_angle = (ind[2] + np.pi) % (2 * np.pi) - np.pi

            # Compute vector from individual to mean position of context
            context_mean = np.array([np.mean(context_x), np.mean(context_y)])
            diff = context_mean - ind[0:2]

            # Calculate differences between actual and expected angles
            expected_angle = np.arctan2(diff[1], diff[0])
            raw_diff = abs(expected_angle - actual_angle)
            true_diff = min(raw_diff, 2*np.pi - raw_diff) * 180 / np.pi

            # Print result
            if true_diff <= args.error_margin:
                print "Scene {} is VALID".format(index+1)
            else:
                print "Scene {} is INVALID".format(index+1)

