import numpy as np
from PIL import Image
import sys

# 0 for cocktail party, 1 for warehouse
if len(sys.argv) > 1:
    image = int(sys.argv[1])
else:
    image = 0

if image == 0:
    map_image = Image.open("cocktail_short_map.png")
else:
    map_image = Image.open("warehouse_short_map.png")

map_data = np.asarray(map_image)

group_dim = 40

# origin might be reverse of printout (not sure why)
if image == 0:
    origin = (0, 0) # cocktail party
    map_dim = (6, 5) # cocktail party
else:
    origin = (-20, -20) # warehouse
    map_dim = (40, 40) # warehouse

width = map_data.shape[0]
length = map_data.shape[1]

occupied = 200 # arbitrary cutoff

centers = []

for i in range(width - group_dim + 1):
    for j in range(length - group_dim + 1):
        valid = True
        for r in range(group_dim):
            if not valid:
                break
            for s in range(group_dim):
                if map_data[i+r][j+s][0] <= occupied:
                    valid = False
                    break
        if valid:
            if image == 0:
                x = origin[0] - ((i + group_dim / 2) / width * map_dim[0]) # origin is bottom left, so we have to subtract from the top left
            else:
                x = origin[0] + ((i + group_dim / 2) / width * map_dim[0])
            z = origin[1] + ((j + group_dim / 2) / length * map_dim[1])
            #print((x,z))
            centers.append((x, z))

#print(centers)

import random
import pickle

if image == 0:
    num = 30
else:
    num = 100

spawn_pos = []
for i in range(num):
    next_center = random.choice(centers)
    spawn_pos.append((next_center[0], next_center[1], random.randint(0, 359)))

if image == 0:
    filename = "cocktail_party_spawn_positions"
else:
    filename = "warehouse_spawn_positions"

with open(filename, "w") as fp:
    #pickle.dump(spawn_pos, fp)
    for spawn in spawn_pos:
        output = "{} {} {}\n".format(spawn[0], spawn[1], spawn[2])
        fp.write(output)
