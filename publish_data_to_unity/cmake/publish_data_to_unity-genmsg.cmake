# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "publish_data_to_unity: 2 messages, 0 services")

set(MSG_I_FLAGS "-Ipublish_data_to_unity:/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/melodic/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(publish_data_to_unity_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" NAME_WE)
add_custom_target(_publish_data_to_unity_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "publish_data_to_unity" "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" "geometry_msgs/Pose:geometry_msgs/Quaternion:geometry_msgs/Point"
)

get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" NAME_WE)
add_custom_target(_publish_data_to_unity_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "publish_data_to_unity" "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" "publish_data_to_unity/TrackedPerson:geometry_msgs/Pose:geometry_msgs/Point:geometry_msgs/Quaternion:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/publish_data_to_unity
)
_generate_msg_cpp(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg"
  "${MSG_I_FLAGS}"
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/publish_data_to_unity
)

### Generating Services

### Generating Module File
_generate_module_cpp(publish_data_to_unity
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/publish_data_to_unity
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(publish_data_to_unity_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(publish_data_to_unity_generate_messages publish_data_to_unity_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_cpp _publish_data_to_unity_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_cpp _publish_data_to_unity_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(publish_data_to_unity_gencpp)
add_dependencies(publish_data_to_unity_gencpp publish_data_to_unity_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS publish_data_to_unity_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/publish_data_to_unity
)
_generate_msg_eus(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg"
  "${MSG_I_FLAGS}"
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/publish_data_to_unity
)

### Generating Services

### Generating Module File
_generate_module_eus(publish_data_to_unity
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/publish_data_to_unity
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(publish_data_to_unity_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(publish_data_to_unity_generate_messages publish_data_to_unity_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_eus _publish_data_to_unity_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_eus _publish_data_to_unity_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(publish_data_to_unity_geneus)
add_dependencies(publish_data_to_unity_geneus publish_data_to_unity_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS publish_data_to_unity_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/publish_data_to_unity
)
_generate_msg_lisp(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg"
  "${MSG_I_FLAGS}"
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/publish_data_to_unity
)

### Generating Services

### Generating Module File
_generate_module_lisp(publish_data_to_unity
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/publish_data_to_unity
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(publish_data_to_unity_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(publish_data_to_unity_generate_messages publish_data_to_unity_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_lisp _publish_data_to_unity_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_lisp _publish_data_to_unity_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(publish_data_to_unity_genlisp)
add_dependencies(publish_data_to_unity_genlisp publish_data_to_unity_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS publish_data_to_unity_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/publish_data_to_unity
)
_generate_msg_nodejs(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg"
  "${MSG_I_FLAGS}"
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/publish_data_to_unity
)

### Generating Services

### Generating Module File
_generate_module_nodejs(publish_data_to_unity
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/publish_data_to_unity
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(publish_data_to_unity_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(publish_data_to_unity_generate_messages publish_data_to_unity_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_nodejs _publish_data_to_unity_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_nodejs _publish_data_to_unity_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(publish_data_to_unity_gennodejs)
add_dependencies(publish_data_to_unity_gennodejs publish_data_to_unity_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS publish_data_to_unity_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/publish_data_to_unity
)
_generate_msg_py(publish_data_to_unity
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg"
  "${MSG_I_FLAGS}"
  "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/publish_data_to_unity
)

### Generating Services

### Generating Module File
_generate_module_py(publish_data_to_unity
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/publish_data_to_unity
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(publish_data_to_unity_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(publish_data_to_unity_generate_messages publish_data_to_unity_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPerson.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_py _publish_data_to_unity_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/wh367/catkin_ws/src/embodiment_experiment/publish_data_to_unity/msg/TrackedPersons.msg" NAME_WE)
add_dependencies(publish_data_to_unity_generate_messages_py _publish_data_to_unity_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(publish_data_to_unity_genpy)
add_dependencies(publish_data_to_unity_genpy publish_data_to_unity_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS publish_data_to_unity_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/publish_data_to_unity)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/publish_data_to_unity
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(publish_data_to_unity_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(publish_data_to_unity_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/publish_data_to_unity)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/publish_data_to_unity
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(publish_data_to_unity_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(publish_data_to_unity_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/publish_data_to_unity)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/publish_data_to_unity
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(publish_data_to_unity_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(publish_data_to_unity_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/publish_data_to_unity)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/publish_data_to_unity
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(publish_data_to_unity_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(publish_data_to_unity_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/publish_data_to_unity)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/publish_data_to_unity\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/publish_data_to_unity
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(publish_data_to_unity_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(publish_data_to_unity_generate_messages_py geometry_msgs_generate_messages_py)
endif()
