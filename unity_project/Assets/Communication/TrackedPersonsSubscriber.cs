﻿// Nathan Tsoi 2020

using UnityEngine;
using System.Collections.Generic;
using System.Collections.Concurrent;
using RosSharp.RosBridgeClient.MessageTypes.CocktailpartyToUnity;

namespace RosSharp.RosBridgeClient
{
    public class TrackedPersonsSubscriber : UnitySubscriber<TrackedPersons>
    {
        [SerializeField] public List<GameObject> PersonPrefabs = new List<GameObject>();
        public string PeopleObjectName = "People";
        public bool useRobot = false;
	public float robotHeightOffset = 0.0f;
        public GameObject RobotPrefab;
        public int UseRobotAsPersonId;
        private bool isMessageReceived;
        private GameObject people;
        private ConcurrentQueue<List<TrackedPerson>> tracklists;
        int maxtrack = 1;

        protected override void Start()
        {
            tracklists = new ConcurrentQueue<List<TrackedPerson>>();
            people = GameObject.Find(PeopleObjectName);
			base.Start();
		}
		
        private void Update()
        {
            if (isMessageReceived)
                ProcessMessage();
        }

        protected override void ReceiveMessage(TrackedPersons message)
        {
            List<TrackedPerson> tracks = new List<TrackedPerson>();
            foreach(TrackedPerson track in message.tracks) {
                tracks.Add(track);
            }
            tracklists.Enqueue(tracks);
            isMessageReceived = true;
        }

        private void ProcessMessage()
        {
            List<TrackedPerson> tracks;
            if (!tracklists.TryDequeue(out tracks)) {
                return;
            }

            int track_id = 0;
            foreach(TrackedPerson track in tracks) {
                Vector3 position = GetPosition(track.pose).Ros2Unity();
                Quaternion rotation = GetRotation(track.pose).Ros2Unity();
                track_id = (int)(track.track_id) - 1;
                if (track_id > maxtrack) {
                    maxtrack = track_id;
                }
		Debug.Log(track_id);
                GameObject person = GameObject.Find("Person_"+track_id);
                if (person is null) {
                    if (useRobot && UseRobotAsPersonId == track_id) {
                        person = Instantiate(RobotPrefab, position, rotation);
                        person.name = "Person_" + track_id;
                        person.transform.parent = people.transform;
                    } else {
                        person = Instantiate(PersonPrefabs[track_id], position, rotation);
                        person.name = "Person_" + track_id;
                        person.transform.parent = people.transform;
                    }
		    Rigidbody rb = person.GetComponent<Rigidbody>();
                    rb.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;

         	}

		// normally, the position of humans need to higher than the fetch. so we adjust it here...
		if (useRobot && UseRobotAsPersonId == track_id)			
		    position.y = robotHeightOffset; 

            	person.transform.position = position;
            	person.transform.rotation = rotation;
         
                person.SetActive(true);
            }
            for (int i = track_id+1; i <= maxtrack; i++) {
                GameObject person = GameObject.Find("Person_"+i);
                if (person != null) {
                    person.SetActive(false);
                }
            }
            // TODO track and remove or deactivate all other people
        }

        private Vector3 GetPosition(MessageTypes.Geometry.Pose pose)
        {
            return new Vector3(
                (float)pose.position.x,
                (float)pose.position.y,
                (float)pose.position.z);
        }

        private Quaternion GetRotation(MessageTypes.Geometry.Pose pose)
        {
            return new Quaternion(
                (float)pose.orientation.x,
                (float)pose.orientation.y,
                (float)pose.orientation.z,
                (float)pose.orientation.w);
        }
    }
}
