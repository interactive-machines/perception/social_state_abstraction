﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using RosSharp.RosBridgeClient;

public class KeyboardPublisher : MonoBehaviour
{
    private StringPublisher stringPublisher;

    Dictionary<char, KeyCode> charToKeyCode = new Dictionary<char, KeyCode>() {
        // Letters
        {'a', KeyCode.A},
        {'b', KeyCode.B},
        {'c', KeyCode.C},
        {'d', KeyCode.D},
        {'e', KeyCode.E},
        {'f', KeyCode.F},
        {'g', KeyCode.G},
        {'h', KeyCode.H},
        {'i', KeyCode.I},
        {'j', KeyCode.J},
        {'k', KeyCode.K},
        {'l', KeyCode.L},
        {'m', KeyCode.M},
        {'n', KeyCode.N},
        {'o', KeyCode.O},
        {'p', KeyCode.P},
        {'q', KeyCode.Q},
        {'r', KeyCode.R},
        {'s', KeyCode.S},
        {'t', KeyCode.T},
        {'u', KeyCode.U},
        {'v', KeyCode.V},
        {'w', KeyCode.W},
        {'x', KeyCode.X},
        {'y', KeyCode.Y},
        {'z', KeyCode.Z},

        // Keypad Numbers
        {'1', KeyCode.Keypad1},
        {'2', KeyCode.Keypad2},
        {'3', KeyCode.Keypad3},
        {'4', KeyCode.Keypad4},
        {'5', KeyCode.Keypad5},
        {'6', KeyCode.Keypad6},
        {'7', KeyCode.Keypad7},
        {'8', KeyCode.Keypad8},
        {'9', KeyCode.Keypad9},
        {'0', KeyCode.Keypad0},

        // Arrow Keys
        {'L', KeyCode.LeftArrow},
        {'R', KeyCode.RightArrow},
        {'U', KeyCode.UpArrow},
        {'D', KeyCode.DownArrow}
    };

    void Start()
    {
        stringPublisher = new StringPublisher();
    }

    void Update()
    {
        StringBuilder keysPressed = new StringBuilder();

        foreach(KeyValuePair<char, KeyCode> entry in charToKeyCode)
        {
            if (Input.GetKey(entry.Value))
            {
                keysPressed.Append(entry.Key);
            }
        }

        stringPublisher.publishedString = keysPressed.ToString();
    }
}
