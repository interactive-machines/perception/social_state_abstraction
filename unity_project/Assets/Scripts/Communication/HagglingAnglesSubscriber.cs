using UnityEngine;
using System.Collections;

namespace RosSharp.RosBridgeClient
{
    public class HagglingAnglesSubscriber : UnitySubscriber<MessageTypes.Std.Float32MultiArray>
    {
        public Transform BodyTransform;
	public Transform FaceTransform;

	//private Vector3 position;
	public int timestamp;
	private int timestamp_int;
        private float bodyRotation;
	private float faceRotation;
        private bool isMessageReceived;

        protected override void Start()
        {
            base.Start();
	}

        private void Update()
        {
            if (isMessageReceived) {
                ProcessMessage();
	    }	
        }

        protected override void ReceiveMessage(MessageTypes.Std.Float32MultiArray message)
        {
            //position = GetPosition(message).Ros2Unity();
            bodyRotation = message.data[0];
            faceRotation = message.data[1];
	    timestamp_int = (int)(message.data[2]);
	    isMessageReceived = true;
        }

        private void ProcessMessage()
        {
            //PublishedTransform.position = position;
            BodyTransform.eulerAngles = new Vector3(0, (float)(bodyRotation), 0);
            FaceTransform.eulerAngles = new Vector3(0, (float)(faceRotation), 0);
	    timestamp = timestamp_int;
	}

        /*
        private Vector3 GetPosition(MessageTypes.Geometry.PoseStamped message)
        {
            return new Vector3(
                (float)message.pose.position.x,
                (float)message.pose.position.y,
                (float)message.pose.position.z);
        }

        private Quaternion GetRotation(MessageTypes.Geometry.PoseStamped message)
        {
            return new Quaternion(
                (float)message.pose.orientation.x,
                (float)message.pose.orientation.y,
                (float)message.pose.orientation.z,
                (float)message.pose.orientation.w);
        }
        */
    }
}

