using System.Collections.Generic;
using UnityEngine; 

namespace RosSharp.RosBridgeClient
{
    public class PoseJointSubscriber : UnitySubscriber<MessageTypes.Geometry.PoseArray>
    {
	public GameObject body;

        protected override void ReceiveMessage(MessageTypes.Geometry.PoseArray message)
        {
            for (int i = 0; i < message.poses.Length; i++)
            {

		    Debug.Log(message.poses[i]);
            }
        }
    }
}
