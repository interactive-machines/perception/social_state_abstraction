/*
© CentraleSupelec, 2017
Author: Dr. Jeremy Fix (jeremy.fix@centralesupelec.fr)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Adjustments to new Publication Timing and Execution Framework 
// © Siemens AG, 2018, Dr. Martin Bischoff (martin.bischoff@siemens.com)
// Modified by the IMG for research into context compression

using UnityEngine;
using RosSharp.RosBridgeClient.MessageTypes.PublishDataToUnity;

namespace RosSharp.RosBridgeClient
{
    public class PepperCameraImagePublisher : UnityPublisher<PepperCameraImage>
    {
        public void UpdateMessage(MessageTypes.Sensor.CompressedImage image, ulong frame_num, bool haggling, float body_orient, float face_orient, ulong haggling_msg_stamp, bool haggler_status)
        {
	    Publish(new PepperCameraImage(image, frame_num, haggling, body_orient, face_orient, haggling_msg_stamp, haggler_status));
        }

    }
}
