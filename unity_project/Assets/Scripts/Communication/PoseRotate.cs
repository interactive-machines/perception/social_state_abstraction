using UnityEngine;

// Transform.rotation example.

// Rotate a GameObject using a Quaternion.
// Tilt the cube using the arrow keys. When the arrow keys are released
// the cube will be rotated back to the center using Slerp.

public class PoseRotate : MonoBehaviour
{
    float smooth = 5.0f;
    float tiltAngle = 10.0f;
    //public GameObject obj;
    //public GameObject torso;

    void Awake()
    {
	//obj = GameObject.Find("torso");
	//torso = GameObject.Find("torso");
	//obj.transform.parent = torso.transform;
	//obj.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
     	//obj.transform.Rotate(0.0f, 0.0f, 0.0f, Space.Self);
    }

    void Update()
    {
    	transform.Rotate(0, tiltAngle, 0, Space.Self);
	/*
        // Smoothly tilts a transform towards a target rotation.
        float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle;

        // Rotate the cube by converting the angles into a quaternion.
        Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);

        // Dampen towards the target rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, target,  Time.deltaTime * smooth);
	*/
    }
}
