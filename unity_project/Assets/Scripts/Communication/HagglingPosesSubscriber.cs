using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using RosSharp.RosBridgeClient.MessageTypes.PublishDataToUnity;

namespace RosSharp.RosBridgeClient
{
    public class HagglingPosesSubscriber : UnitySubscriber<HagglingPoses>
    {

        public GameObject[] SpeakingFlagsPrefabs;

        public bool useRobot = true;
        private float robotHeightOffset = 0.8f;
        public GameObject RobotPrefab;
        // randomly choose one of the 3 avatars to become a robot if we want
        static System.Random rnd = new System.Random();
        private int UseRobotAsPersonId = -1;

        public string PeopleObjectName = "People";
        private GameObject people;
        private Vector3 center = new Vector3((float)0, (float)0.07, (float)0);
        public GameObject[] AvatarChoices;
        private const int num_agents = 3;
        private ConcurrentQueue<GameObject> chosenPrefabs =  new ConcurrentQueue<GameObject>();

        // 3 items in list; each is a pos-orientation formation for a subject
        private ConcurrentQueue<List<MessageTypes.Geometry.Pose>> formation_array_q = new ConcurrentQueue<List<MessageTypes.Geometry.Pose>>();
        // 3 items in list; each is a face norm for a subject
        private ConcurrentQueue<List<MessageTypes.Geometry.Pose>> face_array_q =  new ConcurrentQueue<List<MessageTypes.Geometry.Pose>>();
        // 3 items in list; each is an array of joints info (pos-orientation of a joint) for a subject
        private ConcurrentQueue<List<MessageTypes.Geometry.PoseArray>> body_array_q = new ConcurrentQueue<List<MessageTypes.Geometry.PoseArray>>();
        // each list has 3 binary indicators for speaking status, 0 for silent, 1 for speaking
        private ConcurrentQueue<List<float>> speech_array_q = new ConcurrentQueue<List<float>>();

        public bool robot_haggling = false;
        public float body_orient = 0f;
        public float face_orient = 0f;
        public ulong haggling_msg_stamp = 0;
        public ulong buyer_id = 3; // illegal num placeholder; range should be [0, 2]
        public bool haggler_status = false; // whether robot is a buyer (True) or a haggler (False)
        public ulong timestamp = 0;

        private bool isMessageReceived = false;
        private bool changed = false;

        private static readonly Dictionary<int, string> haggling_to_rocketbox = new Dictionary<int, string> {
            {0, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck"},
            // {1, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 L Clavicle"},
            {2, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 L Clavicle/Bip01 L UpperArm"},
            {3, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L Forearm"},
            {4, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1"},
            {6, "Bip01/Bip01 Pelvis/Bip01 L Thigh"},
            {7, "Bip01/Bip01 Pelvis/Bip01 L Thigh/Bip01 L Calf"},
            {9, "Bip01/Bip01 Pelvis/Bip01 R Thigh"},
            {10, "Bip01/Bip01 Pelvis/Bip01 R Thigh/Bip01 R Calf"},
            // {11, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 R Clavicle"},
            {12, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 R Clavicle/Bip01 R UpperArm"},
            {13, "Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm"},
        };
        private List<Transform> orig_trans =  new List<Transform>(); // save the initial transform

        private string path = "../publish_data_to_unity/maps/cocktail_party_spawn_positions"; 
        private float[,] spawn_positions = new float[30, 3];
        private int next_position = 0;

        // queue for processing msgs
        private ConcurrentQueue<ulong> frame_q = new ConcurrentQueue<ulong>();
        private ConcurrentQueue<ulong> buyer_id_q = new ConcurrentQueue<ulong>();
        private ConcurrentQueue<ulong> haggling_msg_stamp_q = new ConcurrentQueue<ulong>();
        
        // data for test model workflow
        private ConcurrentQueue<bool> is_model_prediction_q = new ConcurrentQueue<bool>();
        private ConcurrentQueue<bool> ignore_robot_q = new ConcurrentQueue<bool>();
        private bool is_model_prediction = false;
        private bool ignore_robot = false;

        protected override void Start()
        {
            people = GameObject.Find(PeopleObjectName);
            Debug.Log("Found people object; starting");

            int id = 0;
            foreach (GameObject speaking_flag_prefab in SpeakingFlagsPrefabs) {
                GameObject speaking_flag = Instantiate(speaking_flag_prefab, people.transform.position, Quaternion.identity);
                speaking_flag.name = "SpeakingFlag_" + id;
                speaking_flag.transform.parent = people.transform;
                speaking_flag.transform.localScale = new Vector3(0, 0, 0); // hide first

                Debug.Log("instantiated speaking flag id " + id);
                id++;
            }

    	    System.IO.StreamReader file = new System.IO.StreamReader(path);
    	    string line;
    	    string[] tokens;
    	    int i = 0;
    	    while ((line = file.ReadLine()) != null)
    	    {
        		tokens = line.Split();
        		spawn_positions[i, 0] = float.Parse(tokens[0], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        		spawn_positions[i, 1] = float.Parse(tokens[1], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        		spawn_positions[i, 2] = float.Parse(tokens[2], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        		i++;
    	    }
	       file.Close();
	       Debug.Log("Finished reading spawn positions");

            base.Start();
        }
        
        private void Update()
        {
            if (isMessageReceived)
                ProcessMessage();
        }

        protected override void ReceiveMessage(HagglingPoses message)
        {
            is_model_prediction_q.Enqueue(message.is_model_prediction);

            // position and orientation for all 3 avatars (or just body rotation the robot if is_model_prediction)
            List<MessageTypes.Geometry.Pose> curr_formations = new List<MessageTypes.Geometry.Pose>();
            foreach(MessageTypes.Geometry.Pose curr_formation in message.formation.poses) {
                curr_formations.Add(curr_formation);
            }
            formation_array_q.Enqueue(curr_formations);

            // face norm for all 3 avatars (or just face rotation the robot if is_model_prediction)
            List<MessageTypes.Geometry.Pose> curr_faces = new List<MessageTypes.Geometry.Pose>();
            foreach(MessageTypes.Geometry.Pose curr_face in message.faces.poses) {
                curr_faces.Add(curr_face);
            }
            face_array_q.Enqueue(curr_faces);

            Debug.Log("message.is_model_prediction: " + message.is_model_prediction);

            if (message.is_model_prediction) {
                // if the msg is sent from model prediction, then it'd only set formations and faces for the robot
                isMessageReceived = true;
                return;
            }

            // not model prediction, so continue loading data

            //speech statuses for 3 avatars
            List<float> curr_speech = new List<float>();
            // Debug.Log("creating curr_speech for speech_array_q");
            foreach(float speaking in message.speech_bin) {
                // Debug.Log("speaking: " + speaking);
                curr_speech.Add(speaking);
            }
            speech_array_q.Enqueue(curr_speech);


            // bones for all 3 avatars
            List<MessageTypes.Geometry.PoseArray> curr_bodies = new List<MessageTypes.Geometry.PoseArray>();
            foreach(MessageTypes.Geometry.PoseArray curr_body in message.bodies_all) {
                curr_bodies.Add(curr_body);
            }
            body_array_q.Enqueue(curr_bodies);

            frame_q.Enqueue(message.frame_num);
            buyer_id_q.Enqueue(message.buyer_id);
            haggling_msg_stamp_q.Enqueue(message.header.stamp.secs);
            // Debug.Log("frame_num: " + message.frame_num);

            ignore_robot_q.Enqueue(message.ignore_robot);

            // for some reason unity doesn't get the first few msgs
            if (message.frame_num == 3) {
                Debug.Log("Creating new env for new data sequence");
                // create a new unity env: newly select avatar types, robot choice, and center location

                // from ros, but not actually using it -- just calculating in unity instead
                Debug.Log("use_robot_id: " + message.use_robot_id);
                // UseRobotAsPersonId = rnd.Next(num_agents);
                UseRobotAsPersonId = (UseRobotAsPersonId + 1) % 3; // loop over each position
                Debug.Log("UseRobotAsPersonId is " + UseRobotAsPersonId);

                // choose a new world location (per haggling session), then set people.transform.position
                if (next_position >= spawn_positions.Length) {
                    next_position = 0;
                }
                center = new Vector3(spawn_positions[next_position, 0], center.y, spawn_positions[next_position, 1]);
                // people.transform.rotation = Quaternion.Euler(0, spawn_positions[next_position, 2], 0);
                Debug.Log("new center: " + center);
                next_position++;
            
                HashSet<int> chosen_indices = new HashSet<int>();
                // randomly choose 3 unused avatar prefabs for all available prefabs
                for (int track_id = 0; track_id < num_agents; track_id++) {
                    int avatar_choice = rnd.Next(AvatarChoices.Length);
                    while (chosen_indices.Contains(avatar_choice)) {
                        avatar_choice = rnd.Next(AvatarChoices.Length);
                        // Debug.Log("choice: " + avatar_choice);
                    }
                    Debug.Log("track_id " + track_id + " ; avatar_choice: " + avatar_choice);
                    chosen_indices.Add(avatar_choice);
                    chosenPrefabs.Enqueue(AvatarChoices[avatar_choice]);
                }
                changed = true;
            }
   
            isMessageReceived = true;
        }

        private void ProcessMessage()
        {
            isMessageReceived = false;

            // would always be set if we were in test workflow
            // if is_model_prediction, then this would just have one Pose for robot only (ditto for faces)
            List<MessageTypes.Geometry.Pose> curr_formations;
            if (!formation_array_q.TryDequeue(out curr_formations)) {
                Debug.Log("No formation_array_q dequeue");
                return;
            }
            List<MessageTypes.Geometry.Pose> curr_faces;
            if (!face_array_q.TryDequeue(out curr_faces)) {
                Debug.Log("No face_array_q dequeue");
                return;
            }

            if (!is_model_prediction_q.TryDequeue(out is_model_prediction)) {
                Debug.Log("No is_model_prediction_q dequeue");
                return;
            }
            if (is_model_prediction) {
                // for test model work flow:
                // process model predictions separately, which would include only face and body motion
                MessageTypes.Geometry.Pose body = curr_formations[0];
                MessageTypes.Geometry.Pose face = curr_faces[0];
                // model pred (0, face_orient, 0) and (0, body_orient, 0) stored in pose.position 
                // to convert to Vector3 directly in Unity coord
                Vector3 body_deltaAngles = GetPosition(body);
                Vector3 face_deltaAngles = GetPosition(face);
                GameObject robot = GameObject.Find("Person_" + UseRobotAsPersonId);

                if (robot != null) {
                    // only control robot when sysmte has alrdy instantiated it
                    // Debug.Log("rotate w body_deltaAngles: " + body_deltaAngles);
                    robot.transform.Rotate(face_deltaAngles, Space.World);

                    Transform neck_t = robot.transform.Find("JulietteY20MP/base_link/torso/Neck");
                    if (neck_t != null) {
                        // Debug.Log("rotate w face_deltaAngles: " + face_deltaAngles);
                        neck_t.Rotate(face_deltaAngles, Space.World);
                    } else {
                        Debug.Log("Robot neck transform can't be found for robot id " + UseRobotAsPersonId);
                    }
                    
                } else {
                    Debug.Log("robot not instantiated yet for assigning model prediction");
                }

                Debug.Log("Done processing model prediction");
                return;
            }

            // else process haggling data that
            // if we're in test workflow after the 4th frame, would ignore robot actions
            if (!ignore_robot_q.TryDequeue(out ignore_robot)) {
                Debug.Log("No ignore_robot_q dequeue");
                return;
            }

            List<MessageTypes.Geometry.PoseArray> curr_bodies;
            if (!body_array_q.TryDequeue(out curr_bodies)) {
                Debug.Log("No body_array_q dequeue");
                return;
            }

            ulong frame_num;
            if (!frame_q.TryDequeue(out frame_num)) {
                Debug.Log("No frame_q dequeue");
                return;
            }
            if (!buyer_id_q.TryDequeue(out buyer_id)) {
                Debug.Log("No buyer_id_q dequeue");
                return;
            }
            haggler_status = ((int)buyer_id == UseRobotAsPersonId);

            if (!haggling_msg_stamp_q.TryDequeue(out haggling_msg_stamp)) {
                Debug.Log("No haggling_msg_stamp_q dequeue");
                return;
            }

            List<float> curr_speech;
            if (!speech_array_q.TryDequeue(out curr_speech)) {
                Debug.Log("No speech_array_q dequeue");
                return;
            } else if (curr_speech.Count == 3 && UseRobotAsPersonId >= 0 && UseRobotAsPersonId < 3) {
                robot_haggling = ((int)(curr_speech[UseRobotAsPersonId])) == 1;
                // Debug.Log("robot_haggling: " + robot_haggling);
            }

            int track_id = 0;
            // instantiate all 3 avatars based on formation during this frame
            foreach(MessageTypes.Geometry.Pose curr_formation in curr_formations) {
                if ((UseRobotAsPersonId == track_id) && ignore_robot && !changed) {
                    // in test workflow, so we have handled robot separately
                    Debug.Log("ignoring robot at frame_num " + frame_num);
                    track_id++;
                    continue;

                } else if (UseRobotAsPersonId == track_id && changed) {
                    Debug.Log("changed is true; NOT ignoring robot at frame_num " + frame_num);
                } else if (UseRobotAsPersonId == track_id && !changed) {
                    Debug.Log("changed is NOT true; NOT ignoring robot at frame_num " + frame_num);
                }

                Vector3 position = GetPosition(curr_formation).Ros2Unity();
                Quaternion rotation = GetRotation(curr_formation).Ros2Unity();
                // Debug.Log("id: " + track_id + "; pos: " + position + "; rot: " + rotation);

                GameObject person = GameObject.Find("Person_"+ track_id);
                // the position of humans need to higher than the robot.
                if (useRobot && UseRobotAsPersonId == track_id) {        
                    position.y += robotHeightOffset; 
                }
                // initialization if first time

                bool ready = true;
                if (changed) {
                    // in new scene so initially set all speaking flags to hide
                    for (int id = 0; id < 3; id++) {
                        GameObject speaking_flag = GameObject.Find("SpeakingFlag_" + id);
                        if (speaking_flag != null) speaking_flag.transform.localScale = new Vector3(0, 0, 0); // hide
                    }   
                }
                
                if (person is null || changed) {
                    GameObject curr_prefab;
                    if (!chosenPrefabs.TryDequeue(out curr_prefab)) {
                        Debug.Log("No prefab dequeue");
                        ready = false;
                    } else {
                        ready = true;
                        if (track_id == 0) {
                            people.transform.position = center;
                        }
                        if (track_id == 2) changed = false; // reset
                        // use prefab to instantiate
                        if (useRobot && UseRobotAsPersonId == track_id) {
                            // make a robot
                            Debug.Log("making a new robot in frame_num " + frame_num + " for id " + track_id);
                            GameObject.Destroy(person);
                            person = Instantiate(RobotPrefab, position, rotation);
            
                        } else {
                            // make an avatar
                            GameObject.Destroy(person);
                            person = Instantiate(curr_prefab, position, rotation);
                            
                            Rigidbody currentRb = person.AddComponent<Rigidbody>();
                            if (currentRb != null) {
                                currentRb.useGravity = true;
                                currentRb.isKinematic = true;
                                currentRb.mass = 1E5f;
                            }

                        }

                        person.name = "Person_" + track_id;
                        person.transform.parent = people.transform;
                        person.transform.position = people.transform.position + position;
                        // orientation vectors are in global coordinates, not relative to prev frame
                        person.transform.rotation = rotation;

                        Vector3 rotAngles = rotation.eulerAngles; 
                        // Debug.Log("body rotAngles: " + rotAngles);

                        if (useRobot && UseRobotAsPersonId == track_id) {
                            body_orient = rotAngles.y;
                        }


                        orig_trans.Add(person.transform);
                    }
                    
                } else {

                    // not the first time

                    if (useRobot && UseRobotAsPersonId == track_id) {
                        Debug.Log("changing pos and rotation for robot at frame_num " + frame_num + " ; position: " + position);
                    }

                    person.transform.position = people.transform.position + position;
                    // orientation vectors are in global coordinates, not relative to prev frame
                    // person.transform.rotation = rotation; 
                    Vector3 currAngles = person.transform.rotation.eulerAngles;
                    Vector3 rotAngles = rotation.eulerAngles; 
                    Vector3 deltaAngles = rotAngles - currAngles;
                    // Debug.Log("body currAngles: " + currAngles + " ; rotAngles: " + rotAngles + " ; deltaAngles: " + deltaAngles);
                    
                    person.transform.Rotate(deltaAngles, Space.World);

                    if (UseRobotAsPersonId == track_id) {
                        body_orient = deltaAngles.y;
                        // Debug.Log("body_orient: " + body_orient);
                    }

                }

                if (ready) {
                    // set face orientation for robot; not controlling joints for now
                    if (UseRobotAsPersonId == track_id) {
                        MessageTypes.Geometry.Pose curr_face = curr_faces[track_id];

                        Transform neck_t = person.transform.Find("JulietteY20MP/base_link/torso/Neck");
                        if (neck_t != null) {
                            Quaternion rot = GetRotation(curr_face).Ros2Unity();

                            Vector3 currAngles = neck_t.rotation.eulerAngles;
                            Vector3 rotAngles = rot.eulerAngles; 
                            Vector3 deltaAngles = rotAngles - currAngles;
                            // Debug.Log("face currAngles: " + currAngles + " ; rotAngles: " + rotAngles + " ; deltaAngles: " + deltaAngles);
                            
                            neck_t.Rotate(deltaAngles, Space.World);

                            face_orient = deltaAngles.y;

                            // neck_t.rotation = rot;

                        } else {
                            Debug.Log("Robot neck transform can't be found for track_id " + track_id);
                        }
                    } else {
                        GameObject speaking_flag = GameObject.Find("SpeakingFlag_" + track_id);
                        if (speaking_flag == null) {
                            Debug.Log("speaking flag is null for track_id " + track_id);
                        } else {
                            if (((int)(curr_speech[track_id])) == 1) {
                                // mark the person as speaking
                                // set the flag to appear above the avatar
                                speaking_flag.transform.position = new Vector3(person.transform.position.x, person.transform.position.y + 2, person.transform.position.z);
                                // TODO: switch to variable for test workflow instead of uncommenting this
                                speaking_flag.transform.localScale = new Vector3(0, 0, 0); // hide
                                // speaking_flag.transform.localScale = new Vector3(1, 1, 1); // show
                                Debug.Log("setting speaking flag to appear for avatar " + track_id);
                            } else {
                                // don't mark the person as speaking
                                speaking_flag.transform.localScale = new Vector3(0, 0, 0); // hide
                                Debug.Log("setting speaking flag to disappear for avatar " + track_id);
                            }
                            
                        }
                        

                        // iterate over all bones to control joints for avatar
                        MessageTypes.Geometry.PoseArray curr_body = curr_bodies[track_id];
                        int bone_id = 0;
                        string bone_name;
                        foreach(MessageTypes.Geometry.Pose curr_joint in curr_body.poses) {

                            if (haggling_to_rocketbox.TryGetValue(bone_id, out bone_name)) {
                                Transform t = person.transform.Find(bone_name);
                                if (t != null)
                                {
                                    // update pose
                                    Quaternion rot = GetRotation(curr_joint).Ros2Unity();
                                    t.rotation = rot;
                                    if (bone_id == 0) {
                                        // only using face normal (conceptualy), so not modifying the original head level
                                        // i.e. only rotating face left and right, but not up and down
                                        Vector3 neck_angles = t.localRotation.eulerAngles;
                                        t.localRotation = Quaternion.Euler(neck_angles.x, neck_angles.y, (float)-21.988);
                                    }

                                    // forearm left, right
                                    if (bone_id == 3 || bone_id == 13) {
                                        
                                        Vector3 glob_angles = t.rotation.eulerAngles;
                                        Vector3 loc_angles = t.localRotation.eulerAngles;
                                        if (glob_angles.z > 240) {
                                            t.rotation = Quaternion.Euler(glob_angles.x, glob_angles.y, 240);
                                            glob_angles.z = 240;
                                        }
                                        if (glob_angles.z < 85) {
                                            t.rotation = Quaternion.Euler(glob_angles.x, glob_angles.y, 85);
                                            glob_angles.z = 85;
                                        }
                                        if (glob_angles.y < 84) {
                                            t.rotation = Quaternion.Euler(glob_angles.x, 84, glob_angles.z);
                                            glob_angles.y = 84;
                                        }
                                        // Debug.Log("track_id: " + track_id + " ; bone_id: " + bone_id + "; loc_angles: " + loc_angles + " ; glob_angles: " + glob_angles);
                                    }
                                    // upperarm left, right
                                    if (bone_id == 2 || bone_id == 12) {
                                        
                                        Vector3 glob_angles = t.rotation.eulerAngles;
                                        Vector3 loc_angles = t.localRotation.eulerAngles;
                                        if (glob_angles.z > 95) {
                                            t.rotation = Quaternion.Euler(glob_angles.x, glob_angles.y, 95);
                                            glob_angles.z = 95;
                                        }
                                        if (glob_angles.z < 80) {
                                            t.rotation = Quaternion.Euler(glob_angles.x, glob_angles.y, 80);
                                            glob_angles.z = 80;
                                        }
                                        // Debug.Log("track_id: " + track_id + " ; bone_id: " + bone_id + "; loc_angles: " + loc_angles + " ; glob_angles: " + glob_angles);
                                    }
                                }
                                else {
                                    // Debug.Log(bone_name + " cannot be found for track_id " + track_id);
                                }
                            }  
                            
                            bone_id++;
                        }
                    
                    }
                    person.SetActive(true);
                    track_id++;
                }
            }

	       timestamp = frame_num;
        }

        private Vector3 GetPosition(MessageTypes.Geometry.Pose pose)
        {
            return new Vector3(
                (float)pose.position.x,
                (float)pose.position.y,
                (float)pose.position.z);
        }

        private Quaternion GetRotation(MessageTypes.Geometry.Pose pose)
        {
            return new Quaternion(
                (float)pose.orientation.x,
                (float)pose.orientation.y,
                (float)pose.orientation.z,
                (float)pose.orientation.w);
        }

    }
}
