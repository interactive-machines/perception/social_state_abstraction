# Learning to Orient in Group Conversations via Social State Abstraction

This is the source code for our research on using social state abstraction for learning the motion policy in orienting towards the speaker of a conversation. We leverage the Unity environment, the [Haggling dataset](http://domedb.perception.cs.cmu.edu/ssp), and Microsoft's [Rocketbox Avatar](https://github.com/microsoft/Microsoft-Rocketbox) to create 3D dataset and test environment for the experiments. 

:clapper: Watch a video clip of rolling out in real time VAE's learned motion policy in Unity [here](https://youtu.be/g1-OoavxISM) :clapper:

## Requirements
- Valid ROS distribution (Tested on ROS Melodic with Ubuntu 18.04)
- [Rosbridge Server](http://wiki.ros.org/rosbridge_suite) Linux Package 
- Unity Editor (Tested on Unity version 2019.4.0f1)
- Tensorflow 2.4.1

## Data Collection

- Install the correct unity version (it will prompt in UnityHub)

- Open the `unity_project` folder as a Unity project.

- Initialize scene assets in terminal with:

```
git submodule update --init
```
Note: many of these assets are from the [Microsoft Rocketbox Avatar Library](https://github.com/microsoft/Microsoft-Rocketbox)

- Load the scene from the scenes folder by using `Open Scene` in the Unity editor and selecting `Assets > Scenes > CocktailParty.unity`

- Initialize the catkin workspace 

```
catkin_make install

source ~/catkin_ws/devel/setup.bash
```

- Start roscore

```
roscore
``` 

- Start a rosbridge websocket

```
roslaunch publish_data_to_unity bridge.launch
```

- Hit the play button in the unity scene

- Collect haggling data published to unity

```
cd src/social_state_abstraction/model/data/

./start_record.sh

./collect_data.sh
```

Doing so will create an active rosbag storing the data from the unity scene. For development purposes, after hitting the play button in the unity scene, you can run the following command:

```
rosrun publish_data_to_unity publish_haggling_poses.py --size -1
```

The flag `--size` specifies how many haggling sequences to publish, and the special value `-1` means all the training data sequences. You can also run the following command:

```
rosrun publish_data_to_unity publish_haggling_poses.py --debugging True --filename 170228_haggling_b1_group6.pkl
```

This allows you to run just one specific sequence, whose name is specified through `--filename` for debugging purposes.

You can view the unity through either the game setting, or the scene setting that allows you to zoom in on each character. Examples of each respectively are:

![unity_game](publish_data_to_unity/docs/unity_game.png)

![unity_scene](publish_data_to_unity/docs/unity_scene.png)

To see the images that the robot is capturing, you can run `rosrun rqt_image_view rqt_image_view`. Alternatively, you can just `rviz` and add the `image_raw/compressed` topic. An example of what you might see:

![robot_img](publish_data_to_unity/docs/robot_img.png)


You can also check the frequency of these topics being published through these commands:

```
rostopic hz image_raw/compressed

rostopic hz /pepper_camera/output
```

After data collection is done, you can view the content in the rosbag by playing it through this example command, specifying the name of the rosbag:

```
rosparam set use_sim_time true

rosbag play --clock cocktail_party_2021-04-19-16-48-21.bag
```

## Data Processing

To process the rosbag into npz for model training, run the following example command:

```
./parse_haggling_rosbag.py --bagnames cocktail_party_2021-04-05-01-34-17.bag,warehouse_2021-04-05-07-04-05.bag --filename medium_haggling_dataset.npz
```

The flag `--bagnames` takes a comma separated string for all the rosbags you want to merge into a single npz, whose name is specified through the flag `--filename`. During data processing, we skip the data when the robot is speaking. Additionally, to reduce the data size, we turn the images into gray-scale, and shrink the image size from 960 x 1280 to 96 x 128.

## Model training

To load the data for model training, you can either load it all at once, which will create a cache for faster processing in the future, or you can use a data loader. For the former, run the following command `./main.py --data_file medium_haggling_dataset.npz`. And in the future, you can just pass in the created cache: `./main.py --data_cache cached_medium_haggling_dataset.npz` For the latter, run the following command: `./main.py --generate_data --data_file full_haggling_dataset.npz` for the first time, where for each data sequence it will create a corresponding file under e.g. `full_data/`. In the future, you can just run `./main.py --generator`, which will read the data sequence files in batches. The data loader is especially for when the dataset is too large to fit in memory at one time, but is also more efficient in general. 

During data loading, we process the npz data to create windows of image frames along with the corresponding haggling status (projected to the same dimension as the image frame) for the last frame. You can specify how many frames to skip while creating the windows through `--frame_stride`, such that the window includes more variation in avatar gestures. You can also specify how many frames to skip for gaps between windows through `--stride_size`. 

In `model/main.py`, you can see all the parameters that the model training process take. Here're two common commands we used for training a vae, as the default value for `--model` is `vae` (you could also specify `cnn` for using a convolutional neural network):

```
./main.py --generate_data --data_file full_haggling_dataset.npz --latent_dims 32 --num_epochs 100 --batch_size 32  --model_type 3 --mode 1 --w_img 100.0 --beta 100.0

./main.py --generator --latent_dims 32 --num_epochs 100 --batch_size 32  --model_type 3 --mode 1 --w_img 100.0 --beta 100.0 --w_action 17.0 --best_model logs/cmvae_04-27-2021-16-10_mse_adam/best_weights.ckpt
```

In the first command, we train the vae's encoder and decoder only, without training the action prediction branch, through specifying `--mode` as 1. In the second command, we make all components trainable through specifying `--mode` as 4. We use `--model_type` 3 to choose a specific encoder decoder structure in the model. The flags `--w_img`, `--beta`, and `--w_action` specify the weights for image reconstruction loss, KL divergence loss, and action prediction loss repsectively. We set the first two to be high for better gradient descent, because their values tend to be small. We pass the pre-trained encoder decoder weights through `--best_model`.

To view the training results in tensorboard, you can run:

```
tensorboard --port 6009 --host=0.0.0.0 --logdir logs/
```

This signals tensorboard to read from the model results stored in data folder `logs/`. Then in your browser, go to `<your vm instance address>:6009`. On tensorboard, you can also view visualizations of train and validation images, both the original and the vae's reconstruction. Example images:

![train_orig](publish_data_to_unity/docs/train_orig.png)

![train_recon](publish_data_to_unity/docs/train_recon.png)

## Roll out motion policy

To roll out a model's prediction in real time in the unity scene, first start roscore and rosbridge just like during data collection. Then run the following command:

```
rosrun publish_data_to_unity publish_model_preds.py --model cmvae --best_model cmvae_04-14-2021-23-54_mse_adam  --model_type 3 --latent_dim 32 --mode 4
```

This will load the model and make it ready for taking in robot's images in real time to make predictions. Then hit the play button in the unity scene. Next, start publishing haggling test sequences to the unity scene by running:

```
sh run_test.sh
```

The test workflow adds a floating dot above an avatar if it's speaking. This helps to visualize whether the robot is turning to see the speaker in general. So you might see:

![speaker_dot](publish_data_to_unity/docs/speaker_dot.png)
