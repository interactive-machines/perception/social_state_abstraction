import os
FDIR = os.path.dirname(os.path.abspath(__file__))
import glob
import cv2
import numpy as np
import math
import cv2
import tensorflow as tf


class LoadData():
    def __init__(self, args):
        self.args = args
        if not args.data_dir:
            raise RuntimeError("data_dir must be set")

        self.data_dir = args.data_dir
        self.data_file = args.data_file
        self.data_cache = args.data_cache
        self.withhold = args.withhold

        if self.data_dir[0] is not '/':
            self.data_dir = os.path.join(FDIR, self.data_dir)

        self.model = args.model
        self.train_size = args.train_size
        self.window_len = args.window_len
        self.stride_size = args.stride_size
        self.frame_stride = args.frame_stride
        self.img_h = 96
        self.img_w = 128

        #np.random.seed(1)
        # for classification (cce loss)
        self.classes = 10
        self.bins = np.arange(-np.pi, np.pi, 2*np.pi/self.classes)

        if args.generate_data:
            print("generating full dataset for generator usage")
            self.generate_full_dataset()
            return

        if self.data_cache:
            # load cached file
            self.load_cache()
        else:
            # preprocess
            self.load()

        self.to_np()
        self.to_tf()


    def bin_and_one_hot(self, labels):
        """ convert continous values in self.bins range to one-hot-encoded values """
        return tf.one_hot(np.digitize(labels, self.bins), self.bins.shape[0]).numpy()


    def normalize(images):
        return (tf.cast(images, tf.float32) / 255.0)


    def denormalize(images):
        return tf.cast(255 * images, tf.uint8)


    def generate_input(images, haggler_bit, height=96, width=128, window_len=5):
        # generate the neural network input from a list of images and a haggler bit
        haggling_status = tf.fill([height, width, 1], np.uint8(haggler_bit * 255))
        window_image = tf.reshape(np.asarray(images, dtype=np.uint8), (window_len * height, width, 1))
        joint_tensor = tf.concat([window_image, haggling_status], 0)
        return tf.expand_dims(LoadData.normalize(joint_tensor), 0)  # batch size = 1


    def to_normalized_tuple(self, data_dict):
        # return concatenated image window, latest action (label), and haggling status
        haggling_status = tf.fill([self.img_h, self.img_w, 1], data_dict["haggler_statuses"] * 255)
        joint_tensor = tf.concat([data_dict["img_windows"], haggling_status], 0)
        return (LoadData.normalize(joint_tensor), data_dict["actions"][self.window_len - 1], data_dict["haggler_statuses"])


    def to_normalized_tuple_vae(self, data_dict):
        haggling_status = tf.fill([self.img_h, self.img_w, 1], data_dict["haggler_statuses"] * 255)
        img = tf.concat([data_dict["input"], haggling_status], 0)
        recon_img = tf.concat([data_dict["image"], haggling_status], 0)
        return (LoadData.normalize(img), LoadData.normalize(recon_img))


    def get_cnn_dataset(self, training=True):
        if training:
            dataset_dict = self.train_dict
        else:
            dataset_dict = self.val_dict
        img_windows = dataset_dict["img_windows"]
        haggler_statuses = dataset_dict["haggler_statuses"]
        actions = dataset_dict["actions"]
        dataset_x = []
        dataset_y = []
        for i in range(len(img_windows)):
            haggling_status = tf.fill([self.img_h, self.img_w, 1], haggler_statuses[i] * 255)
            joint_tensor = tf.concat([LoadData.normalize(img_windows[i]),tf.cast(haggling_status, tf.float32)], 0)
            dataset_x.append(joint_tensor)
            dataset_y.append(actions[i][self.window_len - 1])
        return tf.stack(dataset_x), tf.stack(dataset_y)


    def get_window_info(self):
        """pre-compute num of windows for more efficient data loading; also get img width and height """
        sample_img = cv2.imdecode(np.frombuffer(self.data_seqs[0][0][0],dtype=np.uint8), -1)
        self.img_h, self.img_w, = np.asarray(sample_img).shape 

        self.num_windows = 0

        self.num_windows = sum([len(range(0, len(data_seq) - (self.window_len * self.frame_stride) + 1, self.stride_size)) for data_seq in self.data_seqs if len(data_seq) >= self.window_len * self.frame_stride])

        print(f"num_windows: {self.num_windows}, img_h: {self.img_h}, img_w: {self.img_w}") # e.g. 16832 windows for medium (merged)


    def load(self):
        '''
        load raw data and preprocess into windows of images and corresponding windows of [face orient, body orient] from npz
        '''
        numpy_f = os.path.join(self.data_dir, self.data_file)
        loaded = np.load(numpy_f, allow_pickle=True, encoding="bytes")
        self.data_seqs = loaded["arr_0"]
        # e.g. numpy array (45,), each element is a list (a data seq) of tuples; or (211,)

        print("self.data_seqs.shape: {}".format(self.data_seqs.shape)) 

        self.get_window_info()

        self.img_windows = np.empty((self.num_windows, self.window_len * self.img_h, self.img_w, 1), dtype=np.uint8) # an array of img arrs (each element is a window of imgs)
        self.action_windows = np.empty((self.num_windows, self.window_len, 2), dtype=np.float32) # an array of action arrs (each action arr is [face_orient, body_orient])
        self.haggler_statuses = np.empty(self.num_windows, dtype=np.uint8) # an array of haggler_status arrs, each status corresponds to a window of imgs

        window_index = 0
        for index, data_seq in enumerate(self.data_seqs):
            
            if len(data_seq) < self.window_len * self.frame_stride:
                continue # too short
            
            if index % 40 == 0:
                print(f"data_seq index: {index}, len: {len(data_seq)}")
    
            # sliding window to create windows of imgs and actions w/ skipped frames
            end_index = len(data_seq) - (self.window_len * self.frame_stride) + 1
            for window_start in range(0, end_index, self.stride_size):
                # create a window for a valid range
                window_imgs = []
                window_actions = []
                for i in range(0, self.window_len):
                    curr_index = window_start + i * self.frame_stride
                    curr_dp = data_seq[curr_index] # tuple (img, face_orient, body_orient, ...)
                    curr_img = cv2.imdecode(np.frombuffer(curr_dp[0],dtype=np.uint8), -1)
                    window_imgs.append(np.asarray(curr_img))
                    face_rotate = 0.0
                    body_rotate = 0.0
                    for s in range(self.frame_stride):
                        face_rotate += data_seq[curr_index + s][1]
                        body_rotate += data_seq[curr_index + s][2]
                    window_actions.append(np.asarray([np.float32(face_rotate), np.float32(body_rotate)]))

                self.img_windows[window_index] = tf.reshape(np.asarray(window_imgs), (self.window_len * self.img_h, self.img_w, 1))
                self.action_windows[window_index] = np.asarray(window_actions)
                self.haggler_statuses[window_index] = np.uint8(curr_dp[-1])
                window_index += 1

        print("img_windows.shape: {}".format(self.img_windows.shape)) # e.g.  (2456, 5 * 96, 128, 1)
        print("action_windows.shape: {}".format(self.action_windows.shape)) # e.g.  (2456, 5, 2)
        print("haggler_statuses.shape: {}".format(self.haggler_statuses.shape)) # e.g.  (2456,)

        # cache the data for future usage
        cache_name = "cached_" + self.data_file
        cache_path = os.path.join(self.data_dir, cache_name)
        np.savez(cache_path, img_windows=self.img_windows, action_windows=self.action_windows, haggler_statuses=self.haggler_statuses)

    def generate_full_dataset(self):
        '''
        generates full dataset to data/full_dataset
        '''
        numpy_f = os.path.join(self.data_dir, self.data_file)
        loaded = np.load(numpy_f, allow_pickle=True, encoding="bytes")
        self.data_seqs = loaded["arr_0"]
        # e.g. numpy array (45,), each element is a list (a data seq) of tuples; or (211,)

        print("self.data_seqs.shape: {}".format(self.data_seqs.shape)) 

        self.get_window_info()

        self.dataset_y = []

        window_index = 0

        if not os.path.isdir("data/full_dataset"):
            os.mkdir("data/full_dataset")
        for index, data_seq in enumerate(self.data_seqs):
            if len(data_seq) < self.frame_stride * self.window_len:
                continue # too short
            
            if index % 40 == 0:
                print(f"data_seq index: {index}, len: {len(data_seq)}")
    
            # sliding window to create windows of imgs and actions
            end_index = len(data_seq) - (self.window_len * self.frame_stride) + 1

            for window_start in range(0, end_index, self.stride_size):
                # create a window for a valid range
                window_imgs = []
                window_actions = []
                for i in range(0, self.window_len):
                    curr_index = window_start + i * self.frame_stride
                    curr_dp = data_seq[curr_index] # tuple (img, face_orient, body_orient, ...)
                    curr_img = cv2.imdecode(np.frombuffer(curr_dp[0],dtype=np.uint8), -1)
                    window_imgs.append(np.asarray(curr_img))
                    face_rotate = 0.0
                    body_rotate = 0.0
                    for s in range(self.frame_stride):
                        face_rotate += data_seq[curr_index + s][1]
                        body_rotate += data_seq[curr_index + s][2]
                    window_actions.append(np.asarray([np.float32(face_rotate), np.float32(body_rotate)]))

                img_window = tf.reshape(np.asarray(window_imgs), (self.window_len * self.img_h, self.img_w, 1))
                haggling_status = tf.fill([self.img_h, self.img_w, 1], np.uint8(curr_dp[-1]) * 255)
                joint_tensor = tf.concat([LoadData.normalize(img_window), tf.cast(haggling_status, tf.float32)], 0)
                self.dataset_y.append(np.asarray(window_actions)[4])
                cache_path = os.path.join("data/full_dataset", str(window_index))
                np.save(cache_path, joint_tensor)
                window_index += 1
        
        cache_path = os.path.join("data/full_dataset", "actions")
        np.save(cache_path, self.dataset_y)
        print(f"Saved {window_index} files")
        exit()


    def load_cache(self):
        """ load preprocessed windows of data """
        numpy_f = os.path.join(self.data_dir, self.data_cache)
        loaded = np.load(numpy_f)
        self.img_windows = loaded["img_windows"]
        self.action_windows = loaded["action_windows"]
        self.haggler_statuses = loaded["haggler_statuses"]

        print("img_windows.shape: {}".format(self.img_windows.shape))
        print("action_windows.shape: {}".format(self.action_windows.shape))
        print("haggler_statuses.shape: {}".format(self.haggler_statuses.shape))


    def to_np(self):
        """ form train and validation data in np format """
        withholding = int(self.img_windows.shape[0]*self.withhold)
        print("withholding %d for val"%withholding)
        self.val_idx = np.random.choice(self.img_windows.shape[0], size=withholding, replace=False)

        if self.args.loss != 'mse':
            # for classification, not implemented yet
            print("calling bin_and_one_hot")
            self.actions = self.bin_and_one_hot(self.actions)
       
        if self.train_size == -1:
            self.train_size = self.img_windows.shape[0] # no limit -- use full data

        self.train_img_windows = self.img_windows[[z for z in range(self.train_size) if not z in self.val_idx]] 
        self.train_haggler_statuses = self.haggler_statuses[[z for z in range(self.train_size) if not z in self.val_idx]]
        self.train_actions = self.action_windows[[z for z in range(self.train_size) if not z in self.val_idx]] 
        self.val_img_windows = self.img_windows[self.val_idx]
        self.val_haggler_statuses = self.haggler_statuses[self.val_idx]
        self.val_actions = self.action_windows[self.val_idx]

        self.train_dict = { 'img_windows': self.train_img_windows, 'haggler_statuses': self.train_haggler_statuses, 'actions': self.train_actions }
        self.val_dict = { 'img_windows': self.val_img_windows, 'haggler_statuses': self.val_haggler_statuses, 'actions': self.val_actions }
        self.steps_per_epoch = int(self.train_size / self.args.batch_size)
        self.validation_steps = int(self.val_actions.shape[0] / self.args.batch_size)
        

    def to_tf(self):
        print("calling to_tf")

        # for training
        self.train_dataset_batch = (tf.data.Dataset.from_tensor_slices(self.train_dict)
            .map(self.to_normalized_tuple).shuffle(1024).repeat().batch(self.args.batch_size))
        self.valid_dataset_batch = (tf.data.Dataset.from_tensor_slices(self.val_dict)
            .map(self.to_normalized_tuple).shuffle(512).repeat().batch(self.args.batch_size))

        # for reconstruction
        self.vae_train_dict = {"input": self.train_img_windows, "image": self.train_img_windows, 'haggler_statuses': self.train_haggler_statuses}
        self.vae_val_dict = {"input": self.val_img_windows, "image": self.val_img_windows, 'haggler_statuses': self.val_haggler_statuses}
        self.vae_x_train = (tf.data.Dataset.from_tensor_slices(self.vae_train_dict).map(self.to_normalized_tuple_vae).shuffle(1024).repeat().batch(self.args.batch_size))
        self.vae_x_val = (tf.data.Dataset.from_tensor_slices(self.vae_val_dict).map(self.to_normalized_tuple_vae).shuffle(512).repeat().batch(self.args.batch_size))
        
