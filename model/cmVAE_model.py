import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.backend import random_normal
from tensorflow.keras.layers import Lambda
import encoders
import decoders


# model definition class
class cmVAE_model(Model):
    def __init__(self, hps):
        super(cmVAE_model, self).__init__()
        self.hps = hps # hyperparameters

        # create the 3 base models:
        self.q_img = encoders.ImgEncoder_cnn(num_outputs=self.hps["latent_dims"]*2, model_type=self.hps['model_type'], include_top=True)
        self.p_img = decoders.ImgDecoder_cnn(model_type=self.hps['model_type'])
        self.p_action = decoders.ActionDecoder(action_dim=2) # 2 output for predicting face and body orientation

        # Create sampler -- i.e. cutting x into half (learned 2 vecs, one for mean vec and one for std vec)
        self.mean_params = Lambda(lambda x: x[:, : self.hps["latent_dims"]]) # get the first column, of shape (batch_size, latent_dim)
        self.stddev_params = Lambda(lambda x: x[:, self.hps["latent_dims"]:]) # get the second column of x, of same shape as mean


    def call(self, x, training=True):
        # Possible modes for reconstruction:
        # 0: img -> img + action
        # 1: img -> img, so no action policy
        # 2: img -> action but still training encoder while freezing decoder
        # 3: img -> action but freezing encoder + decoder
        # 4: like 0, but sending mean and std as input to action decoder
        x = self.q_img(x, training) # shape of x is: batch_size, latent_dim * 2 (coming from dense2 linear layer)
        means = self.mean_params(x)
        stddev = tf.math.exp(0.5 * self.stddev_params(x))
        eps = random_normal(tf.shape(stddev)) # shape is (batch_size, latent_dim), == means shape[0], shape[1]
        z = means + eps * stddev # thus shape is (batch_size, latent_dim)
       
        if self.hps["mode"] == 0:
            img_recon = self.p_img(z)
            action_recon = self.p_action(z)
            return img_recon, action_recon, means, stddev, z

        elif self.hps["mode"] == 1:
            img_recon = self.p_img(z)
            action_recon = False
            return img_recon, action_recon, means, stddev, z

        elif self.hps["mode"] == 2 or self.hps["mode"] == 3:
            img_recon = False
            action_recon = self.p_action(z)
            return img_recon, action_recon, means, stddev, z

        # send mean and std as input to action decoder
        elif self.hps["mode"] == 4:
            img_recon = self.p_img(z)
            means_std = tf.concat([means, stddev], 1) # [batch_size, 2 * latent_dim]
            action_recon = self.p_action(means_std)
            return img_recon, action_recon, means, stddev, z


    def encode(self, x, training=False):
        x = self.q_img(x, training)
        means = self.mean_params(x)
        stddev = tf.math.exp(0.5 * self.stddev_params(x))
        eps = random_normal(tf.shape(stddev))
        z = means + eps * stddev
        return z, means, stddev


    def decode(self, z=None, means=None, stddev=None):
        # Possible modes for reconstruction:
        # 0: z -> img + action
        # 1: z -> img
        # 2: z -> action
        # 4: like 0, but decoding action using latent dist instead of sampled z

        if self.hps["mode"] == 0:
            img_recon = self.p_img(z)
            action_recon = self.p_action(z)
            return img_recon, action_recon
            
        elif self.hps["mode"] == 1:
            img_recon = self.p_img(z)
            action_recon = False
            return img_recon, action_recon

        elif self.hps["mode"] == 2:
            img_recon = False
            action_recon = self.p_action(z)
            return img_recon, action_recon

        elif self.hps["mode"] == 4:
            eps = random_normal(tf.shape(stddev)) # shape is (batch_size, latent_dim), == means shape[0], shape[1]
            z = means + eps * stddev # thus shape is (batch_size, latent_dim)
            img_recon = self.p_img(z)
            means_std = tf.concat([means, stddev], 1) # [batch_size, 2 * latent_dim]
            action_recon = self.p_action(means_std)
            return img_recon, action_recon

