import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Conv2D, BatchNormalization, Lambda, Concatenate, Conv2DTranspose, Reshape, ReLU, UpSampling2D, Dropout


class ImgDecoder_cnn(Model):
    def __init__(self, model_type):
        super(ImgDecoder_cnn, self).__init__()
        self.model_type = model_type
        self.create_model()

    def call(self, z):

        if self.model_type == 1:
            x = self.dense(z)
            x = self.reshape(x)
            x = self.dt(x)
            x = self.deconv0(x)
            x = self.deconv1(x)
            x = self.deconv2(x)
            x = self.deconv3(x)

        elif self.model_type == 2:
            x = self.dense(z)
            x = self.reshape(x)
            x = self.dt(x)
            x = self.deconv0(x)
            x = self.deconv1(x)
            x = self.deconv2(x)
            x = self.deconv3(x)
            x = self.deconv4(x)
            x = self.deconv5(x)
            x = self.deconv6(x)

        elif self.model_type == 3 or self.model_type == 4 or self.model_type == 5:
            x = self.network(z)

        return x

    def create_model(self):
        print('[ImgDecoder_cnn] Starting create_model')
        conv_reg = tf.keras.regularizers.l2(0.0005)
        
        if self.model_type == 1:
            self.dense = Dense(units=1179648, name='p_img_dense', activation='relu')
            self.reshape = Reshape((6 * 48, 64, 64))
            self.dt = Dropout(0.25)
            self.deconv0 =  UpSampling2D((2, 2))
            # output shape: (None, 6 * 96, 128, 64)

            self.deconv1 = Conv2DTranspose(filters=64, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
             # output shape: (None, 6 * 96, 128, 64)

            self.deconv2 = Conv2DTranspose(filters=32, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
            # output shape: (None, 6 * 96, 128, 32)

            self.deconv3 = Conv2DTranspose(filters=1, kernel_size=3, padding='same', activation='relu')
            # output shape: (None, 6 * 96, 128, 1)

        elif self.model_type == 2:
            self.dense = Dense(units=589824, name='p_img_dense', activation='relu')
            self.reshape = Reshape((6 * 24, 32, 128))
            self.dt = Dropout(0.25)
            self.deconv0 =  UpSampling2D((2, 2))
            # output shape: (None, 6 * 48, 64, 128)

            self.deconv1 = Conv2DTranspose(filters=128, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
             # output shape: (None, 6 * 48, 64, 128)

            self.deconv2 = Conv2DTranspose(filters=128, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
             # output shape: (None, 6 * 48, 64, 128)

            self.deconv3 =  UpSampling2D((2, 2))
            # output shape: (None, 6 * 96, 128, 128)

            self.deconv4 = Conv2DTranspose(filters=64, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
             # output shape: (None, 6 * 96, 128, 64)

            self.deconv5 = Conv2DTranspose(filters=64, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
             # output shape: (None, 6 * 96, 128, 64)

            self.deconv6 = Conv2DTranspose(filters=1, kernel_size=3, padding='same', activation='relu', kernel_regularizer=conv_reg)
             # output shape: (None, 6 * 96, 128, 1)

        elif self.model_type == 3 or self.model_type == 4 or self.model_type == 5:

            dense = Dense(units=147456, name='p_img_dense')
            reshape = Reshape((6 * 12, 16, 128))
            deconv0 = UpSampling2D((2, 2))
            deconv1 = Conv2DTranspose(filters=128, kernel_size=4, strides=1, padding='same', activation='relu')
            deconv2 = Conv2DTranspose(filters=64, kernel_size=5, strides=1, padding='same', activation='relu', dilation_rate=3)
            deconv3 = Conv2DTranspose(filters=64, kernel_size=6, strides=1, padding='same', activation='relu', dilation_rate=2)
            deconv4 = Conv2DTranspose(filters=32, kernel_size=5, strides=2, padding='same', activation='relu', dilation_rate=1)
            deconv5 = Conv2DTranspose(filters=16, kernel_size=5, strides=1, padding='same', activation='relu', dilation_rate=1)
            deconv6 = Conv2DTranspose(filters=8, kernel_size=6, strides=2, padding='same', activation='relu')
            deconv7 = Conv2DTranspose(filters=1, kernel_size=6, strides=1, padding='same', activation='relu')
            deconv8 =  UpSampling2D((2, 2))
            # output shape: (None, 6 * 96, 128, 1)

            self.network = tf.keras.Sequential([
                dense,
                reshape,
                deconv0,
                deconv1,
                deconv2,
                deconv3,
                deconv4,
                deconv5,
                deconv7,
                deconv8], 
                name='p_img')

        print('[ImgDecoder_cnn] Done with create_model')


class ActionDecoder(Model):
    def __init__(self, action_dim):
        super(ActionDecoder, self).__init__()
        self.create_model(action_dim)

    def call(self, z):
        x = self.dense2(z)
        x = self.dense3(x)
        x = self.dense4(x)
        return x

    def create_model(self, action_dim):
        print('[ActionDecoder] Starting create_model')
        dense0 = Dense(units=512, activation='relu')
        dense1 = Dense(units=128, activation='relu')
        self.dense2 = Dense(units=64, activation='relu')
        self.dense3 = Dense(units=32, activation='relu')
        self.dense4 = Dense(units=action_dim, activation='linear')

        print('[ActionDecoder] Done with create_model')
