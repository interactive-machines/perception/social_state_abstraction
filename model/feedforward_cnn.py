import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, Conv2D, BatchNormalization, Lambda, Concatenate, Conv2DTranspose, Reshape, ReLU, MaxPooling2D, Dropout

# Based on the DroNet
class FeedforwardCNN(Model):
    def __init__(self, model_type=1, num_outputs=2):
        super(FeedforwardCNN, self).__init__()
        self.model_type = model_type
        self.create_model(num_outputs)

    def call(self, img):
        if self.model_type == 1:
            # Input
            x1 = self.conv0(img)
            x1 = self.max0(x1)

            # First residual block
            x2 = self.bn0(x1)
            # x2 = x1
            x2 = tf.keras.layers.Activation('relu')(x2)
            #x2 = tf.keras.layers.Dropout(0.2)(x2)
            x2 = self.conv1(x2)

            x2 = self.bn1(x2)
            x2 = tf.keras.layers.Activation('relu')(x2)
            #x2 = tf.keras.layers.Dropout(0.2)(x2)
            x2 = self.conv2(x2)

            x1 = self.conv3(x1)
            x3 = tf.keras.layers.add([x1, x2])

            # Second residual block
            x4 = self.bn2(x3)
            # x4 = x3
            x4 = tf.keras.layers.Activation('relu')(x4)
            #x4 = tf.keras.layers.Dropout(0.2)(x4)
            x4 = self.conv4(x4)

            x4 = self.bn3(x4)
            x4 = tf.keras.layers.Activation('relu')(x4)
            #x4 = tf.keras.layers.Dropout(0.2)(x4)
            x4 = self.conv5(x4)

            x3 = self.conv6(x3)
            x5 = tf.keras.layers.add([x3, x4])

            # Third residual block
            x6 = self.bn4(x5)
            # x6 = x5
            x6 = tf.keras.layers.Activation('relu')(x6)
            #x6 = tf.keras.layers.Dropout(0.2)(x6)
            x6 = self.conv7(x6)

            x6 = self.bn5(x6)
            x6 = tf.keras.layers.Activation('relu')(x6)
            #x6 = tf.keras.layers.Dropout(0.2)(x6)
            x6 = self.conv8(x6)

            x5 = self.conv9(x5)
            x7 = tf.keras.layers.add([x5, x6])

            x = tf.keras.layers.Flatten()(x7)

            x = tf.keras.layers.Activation('relu')(x)
            #x = tf.keras.layers.Dropout(0.2)(x)
        elif self.model_type == 2:
            x = tf.image.grayscale_to_rgb(img) # resnet requires 3 channels
            x = self.resnet(x)
            x = Flatten()(x)
        elif self.model_type == 3:
            x = tf.image.grayscale_to_rgb(img) # resnet requires 3 channels
            x = tf.keras.applications.resnet.preprocess_input(x)
            x = self.resnet(x)
            x = Flatten()(x)
        else:
            x1 = self.conv0(img)
            x2 = self.conv1(x1)
            x3 = self.max0(x2)
            x4 = self.conv2(x3)
            x5 = self.conv3(x4)
            x6 = self.max1(x5)
            x = Flatten()(x6)

        x = self.dense0(x)
        x = self.dropout0(x)
        x = self.dense1(x)
        actions = self.dense2(x)

        return actions

    def create_model(self, num_outputs):
        if self.model_type == 1:
            print('[FeedforwardCNN] Creating DroNet')
            # DroNet
            self.max0 = tf.keras.layers.MaxPooling2D(pool_size=2, strides=2)  # default pool_size='2', strides=2

            self.bn0 = tf.keras.layers.BatchNormalization()
            self.bn1 = tf.keras.layers.BatchNormalization()
            self.bn2 = tf.keras.layers.BatchNormalization()
            self.bn3 = tf.keras.layers.BatchNormalization()
            self.bn4 = tf.keras.layers.BatchNormalization()
            self.bn5 = tf.keras.layers.BatchNormalization()

            self.conv0 = Conv2D(filters=32, kernel_size=5, strides=2, padding='same', activation='linear')
            self.conv1 = Conv2D(filters=32, kernel_size=3, strides=2, padding='same', activation='linear', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-4))
            self.conv2 = Conv2D(filters=32, kernel_size=3, strides=1, padding='same', activation='linear', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-4))
            self.conv3 = Conv2D(filters=32, kernel_size=1, strides=2, padding='same', activation='linear')
            self.conv4 = Conv2D(filters=64, kernel_size=3, strides=2, padding='same', activation='linear', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-4))
            self.conv5 = Conv2D(filters=64, kernel_size=3, strides=1, padding='same', activation='linear', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-4))
            self.conv6 = Conv2D(filters=64, kernel_size=1, strides=2, padding='same', activation='linear')
            self.conv7 = Conv2D(filters=128, kernel_size=3, strides=2, padding='same', activation='linear', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-4))
            self.conv8 = Conv2D(filters=128, kernel_size=3, strides=1, padding='same', activation='linear', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-4))
            self.conv9 = Conv2D(filters=128, kernel_size=1, strides=2, padding='same', activation='linear')
        elif self.model_type == 2:
            print('[FeedforwardCNN] Creating ResNet without weights')
            # ResNet without weights
            self.resnet = tf.keras.applications.ResNet101(
                include_top=False, weights=None, input_tensor=None, input_shape=None,
                pooling='max', classes=None
            )
        elif self.model_type == 3:
            print('[FeedforwardCNN] Creating ResNet with ImageNet weights')
            # ResNet with image net weights
            self.resnet = tf.keras.applications.ResNet50(
                include_top=False, weights="imagenet", input_tensor=None, input_shape=None,
                pooling='max', classes=None
            )
        else:
            print('[FeedforwardCNN Creating small model')
            self.conv0 = Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same', name='block1_conv1')
            # (None, 6 * 96, 128, 64)
            self.conv1 = Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same', name='block1_conv2')
            # (None, 6 * 96, 128, 64)
            self.max0 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block1_pool')
            # (None, 6 * 48, 64, 64)
            #self.dt = Dropout(0.25)
            
            #Block 2 
            self.conv2 = Conv2D(128, kernel_size=(3, 3), activation='relu', padding='same', name='block2_conv1')
            # (None, 6 * 48, 64, 128)
            self.conv3 = Conv2D(128, kernel_size=(3, 3), activation='relu', padding='same', name='block2_conv2')
            # (None, 6 * 48, 64, 128)
            self.max1 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='block2_pool')

        self.dense0 = tf.keras.layers.Dense(units=64, activation='relu', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-3))
        self.dropout0 = tf.keras.layers.Dropout(0.5)
        self.dense1 = tf.keras.layers.Dense(units=32, activation='relu', kernel_initializer='he_normal', kernel_regularizer=tf.keras.regularizers.l2(1e-3))
        self.dense2 = tf.keras.layers.Dense(units=num_outputs, activation='linear', kernel_regularizer=tf.keras.regularizers.l2(1e-3))

        print('[FeedforwardCNN] Done with create_model')