import numpy as np
import os
import tensorflow as tf

from dataset_generator import DataGenerator
from feedforward_cnn import FeedforwardCNN
from load_data import LoadData


class CNNModel(object):
    def __init__(self, args, dataset, logs_dir, lp):
        self.logs_dir = logs_dir
        # h5 format
        self.model_file = os.path.join(self.logs_dir, 'best_model.ckpt')
        # ckpt format
        self.model_weights_file = self.model_file.replace('model', 'weights').replace('h5', 'ckpt')

        self.lp = lp
        self.pretrained_weights = args.best_model

        # hyperparameters
        self.hps = {
            "optimizer": args.optimizer,
            "batch_size": args.batch_size,
            "num_epochs": args.num_epochs,
            "learning_rate": args.learning_rate,
            "loss": args.loss,
            "beta": args.beta,
            "latent_dims": args.latent_dims,
            "mode": args.mode,
            "model_type": args.model_type
        }
        # dataset
        self.dataset = dataset
        # model
        self.model = FeedforwardCNN(self.hps["model_type"])
        # generator
        self.generator = args.generator

        # Optimizer
        if self.hps['optimizer'] == 'adam':
            self.optimizer = tf.keras.optimizers.Adam(lr=self.hps['learning_rate'])
        else:
            raise ValueError('Unsupported Optimizer!')

        # Loss
        if self.hps['loss'] == 'mse':
            self.loss = tf.keras.losses.MeanSquaredError()
        else:
            raise ValueError('Unsupported Loss!')

    def train(self):
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '0'
        # 0 = all messages are logged (default behavior)
        # 1 = INFO messages are not printed
        # 2 = INFO and WARNING messages are not printed
        # 3 = INFO, WARNING, and ERROR messages are not printed

        # allow growth is possible using an env var in tf2.0
        os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

        self.model.compile(
            optimizer=self.optimizer,
            loss=self.loss
        )

        if self.generator:
            #dataset_size = 193629
            dataset_size = 66026
            self.list_IDs = [i for i in range(dataset_size)]
            np.random.seed(1)
            idx = int(0.8 * len(self.list_IDs))
            train_idxs = self.list_IDs[:idx]
            val_idxs = self.list_IDs[idx:]
            np.random.shuffle(train_idxs)
            np.random.shuffle(val_idxs)
            actions = np.load("data/full_dataset/actions.npy")
            train_generator = DataGenerator(train_idxs, actions, batch_size=self.hps["batch_size"])
            validation_generator = DataGenerator(val_idxs, actions, batch_size=self.hps["batch_size"])
        else:
            train_images, train_actions = self.dataset.get_cnn_dataset(training=True)
            val_images, val_actions = self.dataset.get_cnn_dataset(training=False)
            self.lp.lprint(f"Train images: {len(train_images)}, Train actions: {len(train_actions)}")
            self.lp.lprint(f"Val images: {len(val_images)}, Val actions: {len(val_actions)}")
            self.lp.lprint("Done with loading the data")

        callbacks = [
            tf.keras.callbacks.TensorBoard(log_dir=self.logs_dir, write_images=True),
            tf.keras.callbacks.ModelCheckpoint(
                filepath=self.model_file,
                monitor='val_loss',
                mode='min',
                save_best_only=True,
                save_weights_only=True
            ),
            #PredictionCallback(self.lp, self.list_IDs, actions)
        ]

        if self.generator:
            print("Training with generator")
            history = self.model.fit(
                train_generator,
                epochs=self.hps["num_epochs"], 
                validation_data=validation_generator,
                callbacks=callbacks
            )
            self.model.summary()
            for i in range(10):
                index = np.random.choice(self.list_IDs)
                x = np.load("data/full_dataset/" + str(index) + ".npy")
                x = tf.expand_dims(x, 0)
                y = actions[index]
                y_pred = self.model.predict(x)
                self.lp.lprint(f"prediction [{i}]: {y_pred} | {y} (gt)")
        else:
            history = self.model.fit(
                train_images,
                train_actions,
                batch_size=self.hps["batch_size"],
                epochs=self.hps["num_epochs"], 
                validation_data=(val_images, val_actions),
                callbacks=callbacks,
                shuffle=True
            )

            self.model.summary()

            # do some action prediction printouts
            d = [x for x in self.dataset.valid_dataset_batch.take(1)][0]
            x = d[0] # img; if pairs: TensorShape([32, 2, 256, 256, 3])
            y = d[1] # action; if pairs: TensorShape([32, 2])
            y_pred = self.model(x)
            self.lp.lprint("\nAction predictions after {} epochs".format(self.hps["num_epochs"]))
            for i in range(10):
                j = np.random.randint(0, y.shape[0])
                self.lp.lprint(f"prediction [{j}]: {y_pred[j]} | {y[j]} (gt)")

class PredictionCallback(tf.keras.callbacks.Callback):
    def __init__(self, lp, list_IDs, actions):
        super(PredictionCallback, self).__init__()
        self.lp = lp
        self.list_IDs = list_IDs
        self.actions = actions

    def on_epoch_end(self, epoch, logs=None):
        self.lp.lprint(f'predictions at epoch {epoch}:\n')
        for i in range(20):
            index = np.random.choice(self.list_IDs)
            x = np.load("data/full_dataset/" + str(index) + ".npy")
            x = tf.expand_dims(x, 0)
            y = self.actions[index]
            y_pred = self.model.predict(x)
            self.lp.lprint(f"prediction [{i}]: {y_pred} | {y} (gt)")
