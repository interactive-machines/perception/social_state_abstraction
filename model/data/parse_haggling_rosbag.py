#!/usr/bin/env python3

import os
import numpy as np
import rosbag
import argparse
import cv2

"""
one npz file for the whole dataset, split into sequences

"""
parser = argparse.ArgumentParser(description='Save rosbag in npz format')
parser.add_argument('--bagnames', type=str, default='cocktail_party_2021-04-05-01-34-17.bag,warehouse_2021-04-05-07-04-05.bag',
                    help='a comma delimited list containing names / paths to rosbags to parse')
parser.add_argument('--filename', type=str, default='medium_haggling_dataset.npz',
                    help='name of npz file')
args = parser.parse_args()

file_name = args.filename
prev_frame = -1
dataset = []
data_seq = []
bags = args.bagnames.split(',')
print("bags: ", bags)

def save_data_seq():
    global data_seq, seq_counter
    if len(data_seq) <= 1:
        return
    print(len(data_seq))
    dataset.append(data_seq)
    data_seq = []


def process_img(raw_img):
    curr_img = cv2.imdecode(np.frombuffer(raw_img, dtype = np.uint8), -1)
    gray_img = cv2.cvtColor(curr_img, cv2.COLOR_BGR2GRAY)
    # print('Gray img Dimensions : ', gray_img.shape) # (960, 1280)
    scale_percent = 10 # percent of original size
    width = int(gray_img.shape[1] * scale_percent / 100)
    height = int(gray_img.shape[0] * scale_percent / 100)
    dim = (width, height)
    resized_img = cv2.resize(gray_img, dim, interpolation = cv2.INTER_AREA)
    # print('Resized Dimensions : ',resized_img.shape) # (96, 128)
    # cv2.imshow("Resized image", resized_img)
    # cv2.waitKey(0)
    is_success, img_buf_arr = cv2.imencode(".jpg", resized_img)

    return img_buf_arr.tobytes()


for bagname in bags:
    bag = rosbag.Bag(bagname)

    skipped_first_frame = False
    for topic, msg, time in bag.read_messages(topics=['/pepper_camera/output']):
        if msg.frame_num == prev_frame:
            continue
        elif prev_frame > -1 and msg.frame_num != prev_frame + 1:
            save_data_seq()
            skipped_first_frame = False
        elif msg.haggling and len(data_seq) > 0:
            save_data_seq()
            skipped_first_frame = False
        prev_frame = msg.frame_num
        # filter out data when robot is speaking
        if not msg.haggling:
            # skip first data frame of each data sequence
            if not skipped_first_frame:
                skipped_first_frame = True
            else:
                processed_img = process_img(msg.image.data)
                data_seq.append((processed_img, msg.face_orient, msg.body_orient, msg.image.header.stamp, msg.haggling_msg_stamp, msg.haggler_status))
                # try reloading
                # curr_img = cv2.imdecode(np.frombuffer(processed_img,dtype=np.uint8), -1)
                # print('Resized curr Dimensions : ',curr_img.shape) # (96, 128)
                # cv2.imshow('curr_img',curr_img)
                # cv2.waitKey(0)s
                # cv2.destroyAllWindows()

np.savez(file_name, dataset)
bag.close()

