#!/usr/bin/env python3
#
# experiment with diff models for orientation prediction

import datetime
import numpy as np
import argparse
import os

from load_data import LoadData
import tensorflow as tf
from cmVAE import cmVAE
from cnn_model import CNNModel

# must use a GPU
device_name = tf.test.gpu_device_name()
physical_devices = tf.config.list_physical_devices('GPU') # use tf.config.list_physical_devices('GPU')
if not physical_devices:
  raise SystemError('GPU device not found')
print('Found GPU at: {}'.format(device_name))
# prevent a process from taking up all gpu memory
tf.config.experimental.set_memory_growth(physical_devices[0], True)


class LPrinter():
    def __init__(self, log_file, with_stamp=True):
        self.log = log_file
        if not self.log.endswith('.txt'):
            self.log += '.txt'
        self.stamp = with_stamp

    def lprint(self, msg, mode='a'):
        if self.log is not None:
            stamp = ''
            if self.stamp:
                stamp = "[{}]: ".format(datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"))
            with open(self.log, mode) as logf:
                logf.write("{}{}\n".format(stamp, msg))
        print(msg)


def main():
    parser = argparse.ArgumentParser(description='Main runner for loading data and model training')
    # data loading
    parser.add_argument('--data_dir', type=str, default='data', help='path to data file')
    parser.add_argument('--data_cache', type=str, default=None, help='name of cached / preprocessed data')
    parser.add_argument('--data_file', type=str, default='medium_haggling_dataset.npz', help='name of raw npz data file')
    parser.add_argument('--window_len', type=int, default=5, help='len of window of imgs fed to model')
    parser.add_argument('--stride_size', type=int, default=4, help='stride in sliding window to create img windows for model')
    parser.add_argument('--frame_stride', type=int, default=10, help='stride for subsampling image frames')
    parser.add_argument('--withhold', type=float, default=0.2, help='fraction of data withheld for validation')
    parser.add_argument('--train_size', type=int, default=-1, help="used in datasets.py to_np() for limiting train size")
    parser.add_argument('--generator', action='store_true')
    parser.add_argument('--generate_data', action='store_true')
    # network
    parser.add_argument('--model_type', type=int, default=2, help="adjust cmvae encoder/decoder structure; 1, 2, 3")
    parser.add_argument('--latent_dims', type=int, default=16)
    parser.add_argument('--model', choices=['cmvae', 'cnn'], default='cmvae')
    parser.add_argument('--mode', type=int, default=4, help="0--train everything; 4--train everything but pass distribution to action decoder instead of sampled z vector; 1, 2, 3--freeze encoder / decoder; see cmBAE.py")
    parser.add_argument('--best_model', default=None, help="continue training or loading a trained encoder-decoder for VAE")
    #train
    parser.add_argument('--beta', type=float, default=8.0, help='kl loss weight')
    parser.add_argument('--w_img', type=float, default=2.0, help='img recon loss weight')
    parser.add_argument('--w_action', type=float, default=10.0, help='action loss weight')
    parser.add_argument('--optimizer', choices=['ada', 'adam'], default='adam')
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--num_epochs', type=int, default=50)
    parser.add_argument('--learning_rate', type=float, default=1e-4)
    parser.add_argument('--loss', choices=['mse', 'cce'], default='mse')
    #temporal prior -- not used right now
    parser.add_argument('--temporal_prior', choices=['ba', 'None'], default='None', help="'ba' for bhattacharyya distance for temporal prior")
    parser.add_argument('--temporal_prior_mode', type=int, default=2, help="how many dimensions (starting with the first by default) to impose temporal loss")
    # e.g. 0 if none, 1 if just the first, 2 if both the first and second dimensions
    parser.add_argument('--temporal_prior_first', type=int, default=0, help="whether we count starting with the first; 0 for true, 1 for false")
   
    args = parser.parse_args()

    logs_dir = 'logs/{}_{}_{}_{}'.format(args.model, datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"), args.loss, args.optimizer)
    lp = LPrinter(logs_dir)
    lp.lprint("args:")
    lp.lprint(args)

    if args.generator:
        dataset = None
        print("Using generator so not loading dataset")
    else:
        dataset = LoadData(args)
        print("Not using generator")

    if args.model == 'cmvae': # https://github.com/microsoft/AirSim-Drone-Racing-VAE-Imitation
        print("creating cmVAE...")
        vae_model = cmVAE(args, dataset, logs_dir, lp)
        print("running train_cmvae()...")
        vae_model.train_cmvae()
        print("done running train_cmvae")
    elif args.model == 'cnn':
        print("creating feedforward CNN...")
        cnn_model = CNNModel(args, dataset, logs_dir, lp)
        print("running train()...")
        cnn_model.train()
        print("done running train()")

if __name__ == "__main__":
    main()
