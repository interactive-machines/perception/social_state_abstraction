import numpy as np
import os
import tensorflow as tf

from cmVAE_model import cmVAE_model
from load_data import LoadData


class cmVAE(object):
    def __init__(self, args, dataset, logs_dir, lp):
        self.logs_dir = logs_dir
        # h5 format
        self.model_file = os.path.join(self.logs_dir, 'best_model.h5')
        self.model_action_file = os.path.join(self.logs_dir, 'best_action_model.h5')
        # ckpt format
        self.model_weights_file = self.model_file.replace('model', 'weights').replace('h5', 'ckpt')
        self.model_action_weights_file = self.model_action_file.replace('model', 'weights').replace('h5', 'ckpt')

        self.lp = lp
        self.pretrained_weights = args.best_model

        # hyperparameters
        self.hps = {
            "optimizer": args.optimizer,
            "batch_size": args.batch_size,
            "num_epochs": args.num_epochs,
            "learning_rate": args.learning_rate,
            "loss": args.loss,
            "beta": args.beta,
            "w_img": args.w_img,
            "w_action": args.w_action,
            "latent_dims": args.latent_dims,
            "mode": args.mode,
            "model_type": args.model_type
        }
        # dataset
        self.dataset = dataset
        # model
        self.model = cmVAE_model(self.hps)

        # Optimizer
        if self.hps['optimizer'] == 'adam':
            self.optimizer = tf.keras.optimizers.Adam(lr=self.hps['learning_rate'])
        else:
            raise ValueError('Unsupported Optimizer!')

        # for temporal prior
        self.temporal_prior = args.temporal_prior
        self.temporal_prior_mode = args.temporal_prior_mode # how many dimensions (starting with the first) to impose temporal loss
        self.latent_dims = args.latent_dims
        self.temporal_prior_first = args.temporal_prior_first

        self.generator = args.generator or args.generate_data
        # for when self.dataset is None while using generator
        if self.generator:
            np.random.seed(1)

            if args.train_size == -1:
                # assume full 
                self.train_size = 112095
            else:
                self.train_size = args.train_size

            indices = np.arange(self.train_size)
            np.random.shuffle(indices)
            self.withholding = int(self.train_size * args.withhold)
            self.validation_steps = int(self.withholding / self.hps["batch_size"])
            self.leftover_train = self.train_size - self.withholding
            self.train_steps = int(self.leftover_train / self.hps["batch_size"])
            self.train_indices = indices[:self.leftover_train]
            self.val_indices = indices[self.leftover_train:]

            # hardcoded
            self.dim = (6*96, 128)
            self.n_channels = 1
            self.n_outputs = 2

            # load actions all at once cuz they don't use much memory
            self.action_gts = np.load("data/full_dataset/actions.npy")


    # done per epoch -- shuffle data indices
    def on_epoch_start(self):
        np.random.shuffle(self.train_indices)
        np.random.shuffle(self.val_indices)


    # Generate one batch of data
    def batch_data_generation(self, index, dataset_type, batch_size=None):
        if not batch_size:
            batch_size = self.hps["batch_size"]
        # Generate indexes of the batch
        if dataset_type == "train":
            batch_indices = self.train_indices[index * batch_size:(index+1) * batch_size]
        elif dataset_type == "val":
            batch_indices = self.val_indices[index * batch_size:(index+1) * batch_size]
        else:
            print("unsupported dataset_type for batch_data_generation; only accept 'train' or 'val'")
            exit(1)

        # Generate data
        # Initialization
        X = np.empty((batch_size, *self.dim, self.n_channels))
        y = np.empty((batch_size, self.n_outputs))

        for i, ID in enumerate(batch_indices):
            # Store window of image inputs
            X[i,] = np.load('data/full_dataset/' + str(ID) + '.npy')
            # Store corresponding action target
            y[i,] = self.action_gts[ID]

        return X, y


    def train_cmvae(self):
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '0'
        # 0 = all messages are logged (default behavior)
        # 1 = INFO messages are not printed
        # 2 = INFO and WARNING messages are not printed
        # 3 = INFO, WARNING, and ERROR messages are not printed

        # allow growth is possible using an env var in tf2.0
        os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

        # define metrics
        self.train_loss_rec_img = tf.keras.metrics.Mean(name='train_loss_rec_img')
        self.train_loss_rec_action = tf.keras.metrics.Mean(name='train_loss_rec_action')
        self.train_loss_kl = tf.keras.metrics.Mean(name='train_loss_kl')
        self.validation_loss_rec_img = tf.keras.metrics.Mean(name='validation_loss_rec_img')
        self.validation_loss_rec_action = tf.keras.metrics.Mean(name='validation_loss_rec_action')
        self.validation_loss_kl = tf.keras.metrics.Mean(name='validation_loss_kl')
        if self.temporal_prior != "None":
            self.train_loss_temporal = tf.keras.metrics.Mean(name='train_loss_temporal')
            self.validation_loss_temporal = tf.keras.metrics.Mean(name='validation_loss_temporal')

        metrics_writer = tf.summary.create_file_writer(self.logs_dir)

        mode = self.hps["mode"]
        flag = True
        best_validation_loss = None
        best_val_action_loss = None
        if self.pretrained_weights:
            # load pretrained vae weights
            self.model.load_weights(self.pretrained_weights) # e.g. logs/vaecnn2_03-12-2020-06-22_mse_adam/best_weights.ckpt
            print("finished loading weights from {}".format(self.pretrained_weights))

        # Tensor conversion requested dtype int64 for Tensor with dtype int32: <tf.Tensor 'epoch:0' shape=() dtype=int32>
        # for epoch in tf.range(self.hps["num_epochs"]):
        for epoch in range(self.hps["num_epochs"]):
            if self.generator:
                self.on_epoch_start()
                for index in range(self.train_steps):
                    img_gt, action_gt = self.batch_data_generation(index, "train")
                    self.train(img_gt, action_gt, epoch, mode)

            else:
                for step, train_batch in enumerate(self.dataset.train_dataset_batch.take(self.dataset.steps_per_epoch)):
                    # train_batch is a tuple
                    train_images = train_batch[0] # TensorShape([32, 256, 256, 3]) => for pair: TensorShape([32, 2, 256, 256, 3])
                    train_labels = train_batch[1] # TensorShape([32, 1]) => for pair: TensorShape([32, 2])

                    self.train(train_images, train_labels, epoch, mode)
            
            if flag:
                self.model.q_img.summary()
                self.model.p_img.summary()
                if mode != 1: # if mode == 1 training encoder decoder only; no action prediction
                    self.model.p_action.summary()
                flag = False

            validation_total_loss = 0.0 # the average of all batch losses
            if self.generator:
                for index in range(self.validation_steps):
                    img_gt, action_gt = self.batch_data_generation(index, "val")
                    validation_batch_loss = self.validation(img_gt, action_gt, epoch, mode)
                    validation_total_loss += (1 / self.validation_steps) * validation_batch_loss

            else:
                for step, valid_batch in enumerate(self.dataset.valid_dataset_batch.take(self.dataset.validation_steps)):
                    validation_images = valid_batch[0] # TensorShape([32, 256, 256, 3]) => for pair: TensorShape([32, 2, 256, 256, 3])
                    validation_labels = valid_batch[1] # TensorShape([32, 1]) => for pair: TensorShape([32, 2])
                    validation_batch_loss = self.validation(validation_images, validation_labels, epoch, mode)
                    validation_total_loss += (1 / self.dataset.validation_steps) * validation_batch_loss
            
            # save model for best validation loss and best action validation loss
            if epoch > 5:
                if best_validation_loss is None or validation_total_loss < best_validation_loss:
                    print(f"Validation loss improved from {best_validation_loss} to {validation_total_loss}. Saving model under {self.logs_dir}...")
                    best_validation_loss = validation_total_loss
                    # self.model.save(self.model_file) 
                    # Saving the model to HDF5 format requires the model to be a Functional model or a Sequential model. 
                    # It does not work for subclassed models, because such models are defined via the body of a Python method, 
                    # which isn't safely serializable.
                    self.model.save_weights(self.model_weights_file)
                # cuz if mode == 1, there's no action loss
                if mode != 1 and (best_val_action_loss is None or self.validation_loss_rec_action.result() < best_val_action_loss):
                    print(f"Action validation loss improved from {best_val_action_loss} to {self.validation_loss_rec_action.result()}. Saving model under {self.logs_dir}...")
                    best_val_action_loss = self.validation_loss_rec_action.result()
                    # self.model.save(self.model_action_file)
                    self.model.save_weights(self.model_action_weights_file)

            # Image Reconstruction
            orig_train, recon_train = self.reconstruct('train')
            orig_val, recon_val = self.reconstruct('valid')

            with metrics_writer.as_default():
                tf.summary.scalar('train_loss_rec_img', self.train_loss_rec_img.result(), step=epoch)
                tf.summary.scalar('train_loss_rec_action', self.train_loss_rec_action.result(), step=epoch)
                tf.summary.scalar('train_loss_kl', self.train_loss_kl.result(), step=epoch)
                tf.summary.scalar('validation_loss_rec_img', self.validation_loss_rec_img.result(), step=epoch)
                tf.summary.scalar('validation_loss_rec_action', self.validation_loss_rec_action.result(), step=epoch)
                tf.summary.scalar('validation_loss_kl', self.validation_loss_kl.result(), step=epoch)
                if self.temporal_prior != "None":
                    tf.summary.scalar('train_loss_temporal', self.train_loss_temporal.result(), step=epoch)
                    tf.summary.scalar('validation_loss_temporal', self.validation_loss_temporal.result(), step=epoch)
                
                tf.summary.image("train original", orig_train, step=epoch, max_outputs=16)
                tf.summary.image("train reconstruction", recon_train, step=epoch, max_outputs=16)
                tf.summary.image("val original", orig_val, step=epoch, max_outputs=16)
                tf.summary.image("val reconstruction", recon_val, step=epoch, max_outputs=16)


            print('Epoch {} | TRAIN: L_img: {}, L_action: {}, L_kl: {}, L_tot: {} | validation: L_img: {}, L_action: {}, L_kl: {}, L_tot: {}'
                  .format(epoch, self.train_loss_rec_img.result(), self.train_loss_rec_action.result(), self.train_loss_kl.result(),
                          self.train_loss_rec_img.result()+self.train_loss_rec_action.result()+self.train_loss_kl.result(),
                          self.validation_loss_rec_img.result(), self.validation_loss_rec_action.result(), self.validation_loss_kl.result(),
                          self.validation_loss_rec_img.result() + self.validation_loss_rec_action.result() + self.validation_loss_kl.result()
                          ))

            if self.temporal_prior != "None":
                print(f"Train temporal_loss: {self.train_loss_temporal.result()} | Validation temporal_loss: {self.validation_loss_temporal.result()}")

            if epoch % 5 == 0 and mode != 1: # do some action prediction printouts; when mode = 1, there's no action loss
                x, y = self.batch_data_generation(0, "val")

                if self.temporal_prior != 'None':
                    # just get the firsts' of all the pairs for trying out prediction
                    # TensorShape([32, 256, 256, 3])
                    x = tf.squeeze(tf.slice(x, [0,0,0,0,0],[-1,1,-1,-1,-1]))
                    # TensorShape([32, 1])
                    y = tf.slice(y,[0,0],[-1,1])

                img_recon, y_pred, means, stddev, z = self.model(x, training=False)
                self.lp.lprint(f"\nAction predictions at epoch {epoch}")
                for i in range(10):
                    j = np.random.randint(0, y.shape[0])
                    self.lp.lprint(f"prediction [{j}]: {y_pred[j]} | {y[j]} (gt)")

            self.reset_metrics() # reset all the accumulators of metrics
                

    @tf.function
    def train(self, img_gt, action_gt, epoch, mode):
        # freeze the non-utilized weights

        # train everything from scratch
        if mode == 0 or mode == 4:
            self.model.q_img.trainable = True
            self.model.p_img.trainable = True
            self.model.p_action.trainable = True
        # train encoder and decoder, without training action predictor
        elif mode == 1:
            self.model.q_img.trainable = True
            self.model.p_img.trainable = True
            self.model.p_action.trainable = False
        # train encoder and action, without training decoder
        elif mode == 2:
            self.model.q_img.trainable = True
            self.model.p_img.trainable = False
            self.model.p_action.trainable = True
        # train action only, freezing encoder and decoder
        elif mode == 3:
            self.model.q_img.trainable = False
            self.model.p_img.trainable = False
            self.model.p_action.trainable = True


        if self.temporal_prior != 'None':
            print("temporal prior train")
            # calculate temporal loss through pairs of data points
            with tf.GradientTape() as tape:
                # separate the firsts and seconds in the pairs
                # img_gt shape: TensorShape([32, 2, 256, 256, 3])
                # TensorShape([32, 256, 256, 3])

                print("img_gt shape: ", img_gt.shape)

                img_gt_1 = tf.squeeze(tf.slice(img_gt, [0,0,0,0,0],[-1,1,-1,-1,-1]))
                # TensorShape([32, 1])
                action_gt_1 = tf.slice(action_gt,[0,0],[-1,1])

                print("img_gt_1 shape: ", img_gt_1.shape)
                print("action_gt_1 shape: ", action_gt_1.shape)

                img_gt_2 = tf.squeeze(tf.slice(img_gt, [0,1,0,0,0],[-1,1,-1,-1,-1]))
                action_gt_2 = tf.slice(action_gt, [0,1],[-1,1])

                print("img_gt_2 shape: ", img_gt_2.shape)
                print("action_gt_2 shape: ", action_gt_2.shape)

                img_recon, action_recon, means, stddev, z = self.model(img_gt_1, training=True)
                img_loss, action_loss, kl_loss = self.compute_loss_unsupervised(img_gt_1, action_gt_1, img_recon, action_recon, means, stddev, mode, tf.convert_to_tensor(epoch, dtype=tf.int64))
                img_loss = tf.reduce_mean(img_loss) # 0 if no img_recon
                action_loss = tf.reduce_mean(action_loss) # 0 if no action_recon
                beta, w_img, w_action = self.regulate_weights(tf.convert_to_tensor(epoch, dtype=tf.int64)) # avoid retracing: https://stackoverflow.com/questions/58972225/tensorflow2-warning-using-tffunction
                # beta enforces disentaglement btwn diff features
                total_loss = w_img*img_loss + w_action*action_loss + beta*kl_loss

            gradients = tape.gradient(total_loss, self.model.trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))

            with tf.GradientTape() as tape:
                img_recon_2, action_recon_2, means_2, stddev_2, z_2 = self.model(img_gt_2, training=True)
                img_loss_2, action_loss_2, kl_loss_2 = self.compute_loss_unsupervised(img_gt_2, action_gt_2, img_recon_2, action_recon_2, means_2, stddev_2, mode, tf.convert_to_tensor(epoch, dtype=tf.int64))
                img_loss_2 = tf.reduce_mean(img_loss_2)
                action_loss_2 = tf.reduce_mean(action_loss_2)
                total_loss = w_img*img_loss_2 + w_action*action_loss_2 + beta*kl_loss_2

                temporal_loss = self.compute_temporal_loss(means, stddev, means_2, stddev_2)

                total_loss += temporal_loss
                
            self.train_loss_temporal.update_state(temporal_loss)
            self.train_loss_rec_img.update_state(img_loss+img_loss_2)
            self.train_loss_rec_action.update_state(action_loss+action_loss_2)
            self.train_loss_kl.update_state(kl_loss+kl_loss_2)

            gradients = tape.gradient(total_loss, self.model.trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))

        else:
            print("no temporal prior train")
            with tf.GradientTape() as tape:
                img_recon, action_recon, means, stddev, z = self.model(img_gt, training=True)
                img_loss, action_loss, kl_loss = self.compute_loss_unsupervised(img_gt, action_gt, img_recon, action_recon, means, stddev, mode, tf.convert_to_tensor(epoch, dtype=tf.int64))

                img_loss = tf.reduce_mean(img_loss) # 0 if no img_recon
                action_loss = tf.reduce_mean(action_loss) # 0 if no action_recon
                beta, w_img, w_action = self.regulate_weights(tf.convert_to_tensor(epoch, dtype=tf.int64)) # avoid retracing: https://stackoverflow.com/questions/58972225/tensorflow2-warning-using-tffunction
                # weighted_loss_img = calc_weighted_loss_img(img_recon, img_gt)
                # beta enforces disentaglement btwn diff features
                total_loss = w_img*img_loss + w_action*action_loss + beta*kl_loss
                
            self.train_loss_rec_img.update_state(img_loss)
            self.train_loss_rec_action.update_state(action_loss)
            self.train_loss_kl.update_state(kl_loss)

            gradients = tape.gradient(total_loss, self.model.trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))


    @tf.function
    def validation(self, img_gt, action_gt, epoch, mode):
        if self.temporal_prior != 'None':
            # calculate temporal loss based on pairs of data points
            # separate the pairs of data points
            img_gt_1 = tf.squeeze(tf.slice(img_gt, [0,0,0,0,0],[-1,1,-1,-1,-1]))
            # TensorShape([32, 1])
            action_gt_1 = tf.slice(action_gt,[0,0],[-1,1])
            img_gt_2 = tf.squeeze(tf.slice(img_gt, [0,1,0,0,0],[-1,1,-1,-1,-1]))
            action_gt_2 = tf.slice(action_gt, [0,1],[-1,1])
            print("Validation: temporal_prior")
            print("img_gt shape: ", img_gt.shape)
            print("img_gt_1 shape: ", img_gt_1.shape)
            print("action_gt_1 shape: ", action_gt_1.shape)
            print("img_gt_2 shape: ", img_gt_2.shape)
            print("action_gt_2 shape: ", action_gt_2.shape)

            img_recon, action_recon, means, stddev, z = self.model(img_gt_1, training=False)
            img_loss, action_loss, kl_loss = self.compute_loss_unsupervised(img_gt_1, action_gt_1, img_recon, action_recon, means, stddev, mode, tf.convert_to_tensor(epoch, dtype=tf.int64))
            img_loss = tf.reduce_mean(img_loss) # 0 if no img_recon
            action_loss = tf.reduce_mean(action_loss) # 0 if no action_recon
            beta, w_img, w_action = self.regulate_weights(tf.convert_to_tensor(epoch, dtype=tf.int64)) # avoid retracing: https://stackoverflow.com/questions/58972225/tensorflow2-warning-using-tffunction
            # beta enforces disentaglement btwn diff features
            validation_total_loss = w_img*img_loss + w_action*action_loss + beta*kl_loss

            img_recon_2, action_recon_2, means_2, stddev_2, z_2 = self.model(img_gt_2, training=False)
            img_loss_2, action_loss_2, kl_loss_2 = self.compute_loss_unsupervised(img_gt_2, action_gt_2, img_recon_2, action_recon_2, means_2, stddev_2, mode, tf.convert_to_tensor(epoch, dtype=tf.int64))
            img_loss_2 = tf.reduce_mean(img_loss_2)
            action_loss_2 = tf.reduce_mean(action_loss_2)
            validation_total_loss += (w_img*img_loss_2 + w_action*action_loss_2 + beta*kl_loss_2)

            temporal_loss = self.compute_temporal_loss(means, stddev, means_2, stddev_2)
            validation_total_loss += temporal_loss

            self.validation_loss_temporal.update_state(temporal_loss)
            self.validation_loss_rec_img.update_state(img_loss+img_loss_2)
            self.validation_loss_rec_action.update_state(action_loss+action_loss_2)
            self.validation_loss_kl.update_state(kl_loss+kl_loss_2)

        else:
            print("Validation: no temporal prior")
            img_recon, action_recon, means, stddev, z = self.model(img_gt, training=False)
            img_loss, action_loss, kl_loss = self.compute_loss_unsupervised(img_gt, action_gt, img_recon, action_recon, means, stddev, mode, tf.convert_to_tensor(epoch, dtype=tf.int64))
            img_loss = tf.reduce_mean(img_loss)
            action_loss = tf.reduce_mean(action_loss)
            beta, w_img, w_action = self.regulate_weights(tf.convert_to_tensor(epoch, dtype=tf.int64)) # avoid retracing: https://stackoverflow.com/questions/58972225/tensorflow2-warning-using-tffunction
            validation_total_loss = w_img*img_loss + w_action*action_loss + beta*kl_loss

            self.validation_loss_rec_img.update_state(img_loss)
            self.validation_loss_rec_action.update_state(action_loss)
            self.validation_loss_kl.update_state(kl_loss)
            
        return validation_total_loss


    # CUSTOM TF FUNCTIONS
    @tf.function
    def compute_loss_unsupervised(self, img_gt, action_gt, img_recon, action_recon, means, stddev, mode, epoch):
        if epoch < 3:
            prevent_nan = 0.3
        else:
            prevent_nan = 1.0

        # compute reconstruction loss
        if mode == 0 or mode == 4:
            img_loss = tf.losses.mean_squared_error(img_gt, img_recon)
            # img_loss = tf.losses.mean_absolute_error(img_gt, img_recon)
            action_loss = prevent_nan * tf.losses.mean_squared_error(action_gt, action_recon)
            kl_loss = prevent_nan * -0.5 * tf.reduce_mean(tf.reduce_sum((1 + stddev - tf.math.pow(means, 2) - tf.math.exp(stddev)), axis=1))
        elif mode == 1:
            # no action loss
            img_loss = tf.losses.mean_squared_error(img_gt, img_recon)
            action_loss = 0. # no action loss
            kl_loss = prevent_nan * -0.5 * tf.reduce_mean(tf.reduce_sum((1 + stddev - tf.math.pow(means, 2) - tf.math.exp(stddev)), axis=1))
        elif mode == 2 or mode == 3:
            # only action loss
            img_loss = 0.
            kl_loss = 0.
            action_loss = prevent_nan * tf.losses.mean_squared_error(action_gt, action_recon)

        return img_loss, action_loss, kl_loss


    def reset_metrics(self, ):
        self.train_loss_rec_img.reset_states()
        self.train_loss_rec_action.reset_states()
        self.train_loss_kl.reset_states()
        self.validation_loss_rec_img.reset_states()
        self.validation_loss_rec_action.reset_states()
        self.validation_loss_kl.reset_states()


    # currently this isn't actually regulating any weights
    @tf.function
    def regulate_weights(self, epoch):
        # not doing it by schedule for now
        if epoch == 0:
            print("self.hps['beta']: ", self.hps["beta"])
            print("self.hps['w_img']: ", self.hps["w_img"])
            print("self.hps['w_action']: ", self.hps["w_action"])

        return self.hps["beta"], self.hps["w_img"], self.hps["w_action"]


    def reconstruct(self, subset):
        if subset == "train":
            if self.generator:
                orig, _ = self.batch_data_generation(0, "train", batch_size=8)
            else:
                orig = list(self.dataset.vae_x_train.take(1).as_numpy_iterator())[0][0] # shape: (8, 256, 256, 3)
        elif subset == "valid":
            if self.generator:
                orig, _ = self.batch_data_generation(0, "val", batch_size=8)
            else:
                orig = list(self.dataset.vae_x_val.take(1).as_numpy_iterator())[0][0] # shape: (8, 256, 256, 3)
        
        else:
            print("Unsupported subset!")

        img_recon, action_recon, means, stddev, z = self.model(orig, training=False)

        orig_denormalized = LoadData.denormalize(orig)
        reconstructed_denormalized= LoadData.denormalize(img_recon)
        return (orig_denormalized, reconstructed_denormalized)
       

    @tf.function
    def calc_weighted_loss_img(self, img_recon, images_np):
        flat_pred = tf.reshape(img_recon, [-1])
        flat_gt = tf.reshape(images_np, [-1])
        error_sq = tf.math.squared_difference(flat_gt, flat_pred)
        softmax_weights = tf.math.exp(error_sq) / tf.reduce_sum(tf.math.exp(error_sq))
        weighted_error_sq = error_sq * softmax_weights
        loss = tf.reduce_sum(weighted_error_sq)
        return loss


    @tf.function
    def compute_temporal_loss(self, pm, pv, qm, qv):
        """
        bhattacharyya distance: measures similarity between 2 distributions over the same space

        pm, pv, qm, qv:
        means, stddev, means_2, stddev_2 all have shape: [batch_size, latent_dim] e.g. [32,2]

        according to how the code (from cmvae) implements kl divergence with exp(stddev), 
        when in the formula it's exp(sigma), it seems like stddev is used as variance in this case, 
        so we'll have pv and qv like that too, transforming it from [pv1, pv2] to a diagonal matrix where diagonal entries are pv1 and pv2

        two gaussians with diagonal covariance; parameterized with pm, pv, qm, qv
        computes either:
        - a single Gaussian pm,pv, or
        - a set of Gaussians qm,qv.
        """
        print("temporal loss computing...")
        if self.temporal_prior_mode == 0:
            # don't impose temporal loss
            return 0.0

        elif self.temporal_prior_mode < self.latent_dims:
            # cut the "tails" away e.g. the second latent dimension or the first
            if self.temporal_prior_first == 0:
                # count from beginning
                pm = tf.slice(pm,[0,0],[-1,self.temporal_prior_mode]) # e.g. [batch_size, 1]
                qm = tf.slice(qm,[0,0],[-1,self.temporal_prior_mode])
                pv = tf.slice(pv,[0,0],[-1,self.temporal_prior_mode])
                qv = tf.slice(qv,[0,0],[-1,self.temporal_prior_mode])
                print("counting from first")
                print("pm.shape: ", pm.shape)
                print("pv.shape: ", pv.shape)
                """
                pm.shape:  (8, 1)
                pv.shape:  (8, 1)
                """
            else:
                # count from the end e.g. use the second latent dim and discard the first
                start_index = self.latent_dims - self.temporal_prior_mode # e.g. has 3, use 1, start at 2
                pm = tf.slice(pm,[0,start_index],[-1,-1])
                qm = tf.slice(qm,[0,start_index],[-1,-1])
                pv = tf.slice(pv,[0,start_index],[-1,-1])
                qv = tf.slice(qv,[0,start_index],[-1,-1]) 
                print("counting from last")
                print("pm.shape: ", pm.shape)
                print("pv.shape: ", pv.shape)

        else:
            print("using all dims for temporal loss")
        # compare btwn two sets of (multiple) distributions
        dim = len(qm.shape)
        if (dim == 2):
            axis = 1
        elif dim == 1:
            # compare btwn 2 single distributions
            axis = 0
        else:
            raise RuntimeError(f"compute_temporal_loss doesn't support len(tf.shape(qm)): {dim}")

        diff = qm - pm # shape: (32, 2)

        # interpolated variances
        pqv = (pv + qv) / 2. # shape: (32, 2)

        # log determinants of pv, qv, pqv
        ldpv = tf.reduce_sum(tf.math.log(pv)) # reduced along both rows and cols, so result is a single num
        ldqv = tf.reduce_sum(tf.math.log(qv), axis) # reduced along col so result shape: (32,)
        ldpqv = tf.reduce_sum(tf.math.log(pqv), axis) # shape (32,)

        # shape component based on covariances only
        norm = 0.5 * (ldpqv - 0.5 * (ldpv + ldqv)) # shape (32,)

        # divergence component: i.e. scaled mahalanobis distsance
        dist = 0.125 * tf.reduce_sum(diff * (1./pqv) * diff, axis) # shape (32,)
        # explanation: matmul in this case would be equivalent to element-wise, cuz diagonal matrix, then dot prod

        state_change = dist + norm # this is bhattacharyya distance, which we use to measure state change
        # for batch size = 32, shape is (32,), each entry is the state change for that data point
        
        # take the average of the squared magnitude of the state change: E[delta_state^2]?
        # try just taking the abs val first

        temporal_loss = tf.reduce_mean(tf.math.abs(state_change))
        return temporal_loss

